package main.java.com.javarush.service;

import main.java.com.javarush.controller.Property;
import main.java.com.javarush.exceptions.BookSystemException;
import main.java.com.javarush.model.Constants;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriter.MaxFieldLength;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class LuceneSearcher {
    private Property prop;
    private Path indexDir;
    private Path rootDir;
	private IndexReader reader = null;
	private IndexWriter writer = null;
	private QueryParser parser = null;
	private IndexSearcher searcher = null;
    private final String NAME = "name";
    private final String ID = "id";
    private final String FAVORITE = "favorite";

    private List<String> filesFromRoot = new ArrayList<String>();
    private List<File> filesInRoot = new ArrayList<File>();
    private List<String> extensions;

    public LuceneSearcher() throws BookSystemException, IOException {
        prop = Property.getInstance();
        indexDir = Paths.get(Property.INDEX_DIR);
        rootDir = Paths.get(prop.rootDir);
        extensions = prop.bookExtensions;
	    parser = new QueryParser(NAME, new StandardAnalyzer());
        updateRootIndex();
    }

    public void updateRootIndex() throws BookSystemException {
        rootDir = Paths.get(Property.getInstance().rootDir);
        try {
            Files.createDirectories(indexDir);
            Files.setAttribute(indexDir, "dos:hidden", true);

            if(isIndexExist()) {
                checkIndex();
            } else {
                getBookNamesFromRoot(rootDir.toFile());
                for (File file:filesInRoot){
                    createIndex(file);
                }
            }

	        closeIndexWriter();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createIndex(File file) {
        try {
	        Document doc = getDocument(file, Constants.NO.toString());

	        writer = getIndexWriter(true);
            writer.addDocument(doc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateIndex(File file, String option) {
        try {

	        String bookName = getBookName(file);
	        String id = getId(bookName);
            Document doc = getDocument(file, option);

            if(isContainBook(bookName)) {
                removeIndex(id);
            }
	        writer = getIndexWriter(false);
	        writer.addDocument(doc);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

	private void checkIndex() throws IOException {
		List<String> filesFromIndex = getBookNamesFromIndex();
		getBookNamesFromRoot(rootDir.toFile());

		for (int i = 0; i < filesFromRoot.size(); i++){
			String name = filesFromRoot.get(i);
			if (!filesFromIndex.contains(name)){
				updateIndex(filesInRoot.get(i), Constants.NO.toString());
			} else filesFromIndex.remove(name);
		}
		for(String s : filesFromIndex) {
			removeIndex(getId(s));
		}
		filesFromIndex.clear();
		filesFromRoot.clear();
		filesInRoot.clear();
	}

    private String getBookName(File file) {
	    return file.getName();
    }

    public String getId(String name) {
        return "i" + String.valueOf(name.hashCode());
    }

    public void searchIndex(String searchString, List<File> tableBooks) {
        try {
	        if (!isIndexExist()) return;
	        reader = getIndexReader();
	        searcher = getIndexSearcher();

	        TopDocs results = getSearchResult(searchString + "*");

            tableBooks.clear();

            for (ScoreDoc hits : results.scoreDocs) {
                Document doc = searcher.doc(hits.doc);
                String path = doc.get("path");
                File file = new File(path);
                tableBooks.add(file);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	public TopDocs getSearchResult(String searchString) {
		TopDocs results = null;
		try {
			searcher = getIndexSearcher();
			Query query = parser.parse(searchString);
			results = searcher.search(query, 100);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return results;
	}

    public void removeIndex(String forRemove) {
        try {

	        writer = getIndexWriter(false);
	        //we have to delete by untokenized field
	        Term term = new Term(ID, forRemove);
	        writer.deleteDocuments(term);
	        closeIndexWriter();
	        closeIndexSearcher();

        } catch(Exception e){
            e.printStackTrace();
        }
    }

    public boolean isIndexExist() throws IOException {
        return IndexReader.indexExists(getDirectory());
    }


    private void getBookNamesFromRoot(File dir) {
        BookDataProvider provider = new BookDataProvider();
        try {
            provider.scanFileSystem(dir.getAbsolutePath(), extensions, "", "");
        } catch (BookSystemException e) {
            e.printStackTrace();
        }
        filesInRoot = provider.getScannedBooks();
        for (File file: filesInRoot) filesFromRoot.add(file.getName());
    }

    private List<String> getBookNamesFromIndex() throws IOException {
        List<String> names = new ArrayList<String>();
        reader = getIndexReader();

        int num = reader.maxDoc();

        for(int i = 0; i < num; i++) {
            names.add(reader.document(i).get(NAME));
        }
        return names;
    }

    public boolean isContainBook(String name) throws IOException {
        if (!isIndexExist()) return false;
        reader = getIndexReader();
        int num = reader.maxDoc();
	    boolean result = false;
        for(int i = 0; i < num; i++) {
            if(name.equals(reader.document(i).get(NAME))) {
                result = true;
	            break;
            }
        }
        return result;
    }

    public void setBookFavoriteOption(String bookName, String option) {
	    try {
		    List<Document> docs = getDocumentFromSearch(bookName);
		    writer = getIndexWriter(false);
		    for(Document d : docs) {
			    File file = new File(d.get("path"));
			    updateIndex(file, option);
		    }
		    closeIndexWriter();
	    } catch (IOException e) {
		    e.printStackTrace();
	    }
    }

	public void setBookAsFavorite(String bookName) {
		setBookFavoriteOption(bookName, Constants.YES.toString());
	}

    public void removeBookFromFavorites(String bookName) {
	    setBookFavoriteOption(bookName, Constants.NO.toString());
    }

	private List<Document> getDocumentFromSearch(String bookName) {
		List<Document> docList = new ArrayList<Document>();

		try {
			searcher = getIndexSearcher();
            TopDocs result = getSearchResult(NAME + ":" + "\"" + bookName+ "\"");
			for(ScoreDoc hits : result.scoreDocs) {
				Document doc = searcher.doc(hits.doc);
				docList.add(doc);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return docList;
	}

	public boolean bookIsFavorite(String name) throws IOException{
		List<Document> docs = getDocumentFromSearch(name);
		for(Document d : docs) {
			String value = d.getField(FAVORITE).stringValue();
			if (value.equals(Constants.YES.toString())) return true;
		}
		return false;
	}

	public IndexReader getIndexReader() throws IOException{
		if (reader == null) {
			if (writer != null) {
				closeIndexWriter();
			}
			reader = IndexReader.open(getDirectory());
		}
		return reader;
	}

	public void closeIndexReader() throws IOException{
		if (reader != null) {
			reader.flush();
			reader.close();
			reader = null;
		}
	}

	public IndexWriter getIndexWriter(boolean create) throws IOException{
		if (writer == null) {
			if (reader != null) {
				closeIndexReader();
			}
			writer = new IndexWriter(getDirectory(), new StandardAnalyzer(), create, MaxFieldLength.UNLIMITED);
		}
		return writer;
	}

	public void closeIndexWriter() throws IOException {
		if (writer != null) {
			writer.optimize();
			writer.close();
			writer = null;
		}
	}

	public IndexSearcher getIndexSearcher() throws IOException {
		if (searcher == null) {
			searcher = new IndexSearcher(getDirectory());
		}
		return searcher;
	}

	public void closeIndexSearcher() throws IOException {
		if (searcher != null) {
			searcher.close();
			searcher = null;
		}
	}

	public Document getDocument(File file, String option) {
		Document doc = new Document();
		String bookName = getBookName(file);
		String id = getId(bookName);

		doc.add(new Field(ID, id, Field.Store.YES, Field.Index.UN_TOKENIZED));
		doc.add(new Field(NAME, bookName, Field.Store.YES, Field.Index.ANALYZED));
		doc.add(new Field(FAVORITE, option, Field.Store.YES, Field.Index.ANALYZED));
		doc.add(new Field("path", file.getPath(), Field.Store.YES, Field.Index.ANALYZED));

		return doc;
	}

	public List<File> getFavoriteBooks(){
		List<File> list = new ArrayList<>();
		try {
            if(isIndexExist()) {
                reader = getIndexReader();
                int num = reader.maxDoc();
                for(int i = 0; i < num; i++)
                    if (bookIsFavorite(reader.document(i).get(NAME))){
                        String path = reader.document(i).get("path");
                        File file = new File(path);
                        list.add(file);
                    }
            }
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}

	private Directory getDirectory() throws IOException{
		return FSDirectory.getDirectory(indexDir.toFile());
	}

    public void clearIndex(){
        try {
	        if (!isIndexExist()) return;
            writer = getIndexWriter(false);
            writer.expungeDeletes();
            closeIndexWriter();
	        closeIndexReader();
	        closeIndexSearcher();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}