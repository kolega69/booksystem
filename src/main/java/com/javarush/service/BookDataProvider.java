package main.java.com.javarush.service;

import main.java.com.javarush.exceptions.BookSystemException;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class BookDataProvider {
    private List<File> scannedBooks = new ArrayList<File>();

    public void scanFileSystem(String paths, List<String> extensions, String exceptPath,
                               String rootDir) throws BookSystemException
    {
        scannedBooks.clear();

        if (paths == null || extensions == null) {
            throw new BookSystemException("ERROR! \n" +
                    "Not all parameters are available");
        }
        FileVisitor<Path> fileProcessor = new ProcessFile(exceptPath, extensions, rootDir);
        String[] paths_mass = paths.split(";");
        for (String path : paths_mass) {
            try {
                Files.walkFileTree(Paths.get(path), fileProcessor);
            } catch (IOException e) {
                throw new BookSystemException("ERROR! \n" +
                        path + " does not exists. \n" + "" +
                        "Please, delete this path from config.");
            }
        }
    }

    public List<File> getScannedBooks() {
        return scannedBooks;
    }

    private final class ProcessFile extends SimpleFileVisitor<Path> {
        private List<String> extensions;
        private List<Path> exceptPath;
        private String rootDir;

        public ProcessFile(String exceptPath, List<String> extensions, String rootDir) {
            this.extensions = extensions;
            this.exceptPath = new LinkedList();
            String[] paths = exceptPath.split(";");
            for (String s : paths) this.exceptPath.add(Paths.get(s));
            this.rootDir = rootDir;
        }

        @Override
        public FileVisitResult visitFile(Path aFile, BasicFileAttributes aAttrs) throws IOException {

            String s = aFile.getFileName().toString().toLowerCase();
            String[] str = s.split("\\.");

            if (str.length > 1) {
                if (extensions.contains(str[str.length - 1]))
                    scannedBooks.add(aFile.toFile());
            }

            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path aDir, BasicFileAttributes aAttrs) throws IOException {
            if (exceptPath.contains(aDir) || aDir.equals(Paths.get(rootDir))) {
                return FileVisitResult.SKIP_SUBTREE;
            }
            return FileVisitResult.CONTINUE;
        }
    }
}