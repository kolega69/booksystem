package main.java.com.javarush;

import main.java.com.javarush.controller.MainController;
import main.java.com.javarush.exceptions.BookSystemException;
import main.java.com.javarush.model.Model;
import main.java.com.javarush.service.LuceneSearcher;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws BookSystemException, IOException {
        LuceneSearcher searcher = new LuceneSearcher();
        Model model = new Model();

        MainController controller = new MainController();
        controller.setModel(model);
        controller.setLuceneSearcher(searcher);

        controller.showMainWindow();
        controller.checkIsFoldersExist();
    }
}
