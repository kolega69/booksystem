package main.java.com.javarush.controller.locale;

/**
 * Created by Oleg on 06.03.14.
 */
public enum EKeys {

	OK_BTN("ok.btn"),
	CANCEL_BTN("cancel.btn"),
	PATHS_TAB ("paths.tab"),
	OTHERS_TAB("others.tab"),
	SETTINGS_TITLE("settings.title"),
	PATH_BTN("path.button"),
	ADD_PATH_BTN("add.path.btn"),
	LABEL_NUMBER_OF_ROWS("label.number.of.rows"),
	LABEL_FILE_EXTENSIONS("label.file.extensions"),
	LABEL_EXEPT("label.except"),
	LABEL_HOME("label.home"),
	LABEL_SEARCH("label.search"),
	TABLE_CELL_NAME("table.cell.name"),
	TABLE_CELL_TYPE("table.cell.type"),
	BOOKS_TAB("books.tab"),
	FAVORITES_TAB("favorites.tab"),
	SEARCH_FIELD_TIP("search.text.field.tooltip"),
	SEARCH_FIELD("search.text.field"),
	SCAN_FILES("scanFiles"),
	SETTINGS("settings"),
	RENAME_FILE("rename.file.item"),
	ADD_TO_FAVORITES("add.to.favorites"),
	REMOVE_FROM_FAVORITES("remove.from.favorites"),
	RENAME_NODE("rename.node.item"),
	ADD_NODE("add.node.item"),
	DELETE_NODE("delete.node.item"),
	MOVE_FILES("moveFiles"),
	NEXT_BTN_TITLE("next.btn.title"),
	PREV_BTN_TITLE("prev.btn.title"),
	MENU_TITLE("menu.title"),
	CONFIG_MENU_TITLE("config.menu.title"),
	OPTIONS_MENU_TITLE("options.menu.title"),
	EXIT_MENU_TITLE("exit.menu.title"),
	SCAN_WITH_PATH_MENU_TITLE("scan.with.path"),
	SCAN_MENU_TITLE("scan.menu.title"),
	LANGUAGES_MENU_TITLE("languages.menu.title"),
	HELP_MENU_TITLE("help.menu.title"),
	ABOUT_MENU_TITLE("about.menu.title"),
	OVERWRITE_MASSAGE("overwrite.massage"),
	NOT_CORRECT_MASSAGE("not.correct.massage"),
	DIRECTORY_NOT_EXIST("directory.not.exist"),
	SPECIFY_DIRECTORY("specify.directory"),
    TEXT_ABOUT("about.text.message"),
    NOT_SUPPORT_FORMAT("not.support.format"),
    HELP_IS_ABSENT("help.is.absent"),
    DIRECTORY_CREATING_FILED("directory.creating.filed"),
    RENAMING_FILED("renaming.filed"),
    INCORRECT_FOLDER_NAME("incorrect.folder.name"),
    INCORRECT_FILE_NAME("incorrect.file.name"),
    CAN_NOT_SET_NAME("can.not.set.name"),

	//OptionPane text constants
	OP_WARNING_MESSAGE("OPWarning.message"),
	OPTIONPANE_CANCEL_BUTTON_MNEMONIC("OptionPane.cancelButtonMnemonic"),
	OPTIONPANE_CANCEL_BUTTON_TEXT("OptionPane.cancelButtonText"),
	OPTIONPANE_INPUTDIALOG_TITLE("OptionPane.inputDialogTitle"),
	OPTIONPANE_MESSAGE_DIALOG_TITLE("OptionPane.messageDialogTitle"),
	OPTIONPANE_NO_BUTTON_MNEMONIC("OptionPane.noButtonMnemonic"),
	OPTIONPANE_NO_BUTTON_TEXT("OptionPane.noButtonText"),
	OPTIONPANE_OK_BUTTON_MNEMONIC("OptionPane.okButtonMnemonic"),
	OPTIONPANE_OK_BUTTON_TEXT("OptionPane.okButtonText"),
	OPTIONPANE_TITLE_TEXT("OptionPane.titleText"),
	OPTIONPANE_YES_BUTTON_MNEMONIC("OptionPane.yesButtonMnemonic"),
	OPTIONPANE_YES_BUTTON_TEXT("OptionPane.yesButtonText"),

	//FileChooser text constants
	FILECHOOSER_CHOOSE_BUTTON_TEXT("FileChooser.chooseButtonText"),
	FILECHOOSER_CANCEL_BUTTON_TEXT("FileChooser.cancelButtonText"),
    FILECHOOSER_CANCEL_BUTTON_MNEMONIC("FileChooser.cancelButtonMnemonic"),
    FILECHOOSER_CANCEL_BUTTON_TOOLTIP_TEXT("FileChooser.cancelButtonToolTipText"),
    FILECHOOSER_DIRECTORY_DESCRIPTION_TEXT("FileChooser.directoryDescriptionText"),
    FILECHOOSER_DIRECTORY_OPEN_BUTTON_MNEMONIC("FileChooser.directoryOpenButtonMnemonic"),
    FILECHOOSER_DIRECTORY_OPEN_BUTTON_TEXT("FileChooser.directoryOpenButtonText"),
    FILECHOOSER_DIRECTORY_OPEN_BUTTON_TOOLTIP_TEXT("FileChooser.directoryOpenButtonToolTipText"),
    FILECHOOSER_FILE_DESCRIPTION_TEXT("FileChooser.fileDescriptionText"),
    FILECHOOSER_FILE_SIZE_GIGABYTES("FileChooser.fileSizeGigaBytes"),
    FILECHOOSER_FILESIZE_KILOBYTES("FileChooser.fileSizeKiloBytes"),
    FILECHOOSER_FILESIZE_MEGA_BYTES("FileChooser.fileSizeMegaBytes"),
    FILECHOOSER_HELP_BUTTON_MNEMONIC("FileChooser.helpButtonMnemonic"),
    FILECHOOSER_HELP_BUTTON_TEXT("FileChooser.helpButtonText"),
    FILECHOOSER_HELP_BUTTON_TOOLTIP_TEXT("FileChooser.helpButtonToolTipText"),
    FILECHOOSER_NEWFOLDER_ERROR_SEPARATOR("FileChooser.newFolderErrorSeparator"),
    FILECHOOSER_NEWFOLDER_ERROR_TEXT("FileChooser.newFolderErrorText"),
    FILECHOOSER_OPEN_BUTTON_MNEMONIC("FileChooser.openButtonMnemonic"),
    FILECHOOSER_OPEN_BUTTON_TEXT("FileChooser.openButtonText"),
    FILECHOOSER_OPEN_BUTTON_TOOLTIP_TEXT("FileChooser.openButtonToolTipText"),
    FILECHOOSER_OPEN_DIALOG_TITLE_TEXT("FileChooser.openDialogTitleText"),
    FILECHOOSER_OTHER_NEWFOLDER("FileChooser.other.newFolder"),
    FILECHOOSER_OTHER_NEWFOLDER_SUBSEQUENT("FileChooser.other.newFolder.subsequent"),
    FILECHOOSER_SAVE_BUTTON_MNEMONIC("FileChooser.saveButtonMnemonic"),
    FILECHOOSER_SAVE_BUTTON_TEXT("FileChooser.saveButtonText"),
    FILECHOOSER_SAVE_BUTTON_TOOLTIP_TEXT("FileChooser.saveButtonToolTipText"),
    FILECHOOSER_SAVE_DIALOG_TITLE_TEXT("FileChooser.saveDialogTitleText"),
    FILECHOOSER_UPDATE_BUTTON_MNEMONIC("FileChooser.updateButtonMnemonic"),
    FILECHOOSER_UPDATE_BUTTON_TEXT("FileChooser.updateButtonText"),
    FILECHOOSER_UPDATE_BUTTON_TOOLTIP_TEXT("FileChooser.updateButtonToolTipText"),
    FILECHOOSER_WIN32_NEWFOLDER("FileChooser.win32.newFolder"),
    FILECHOOSER_WIN32_NEWFOLDER_SUBSEQUENT("FileChooser.win32.newFolder.subsequent"),
    FILECHOOSER_ACCEPT_ALLFILE_FILTER_TEXT("FileChooser.acceptAllFileFilterText"),
	FILECHOOSER_HOME_FOLDER_ACCESSIBLE_NAME("FileChooser.homeFolderAccessibleName"),
	FILECHOOSER_HOME_FOLDER_TOOLTIP_TEXT("FileChooser.homeFolderToolTipText"),
	FILECHOOSER_NEW_FOLDER_TOOLTIPTEXT("FileChooser.newFolderToolTipText"),

	FILECHOOSER_UPFOLDER_TOOLTIP_TEXT("FileChooser.upFolderToolTipText"),
	FILECHOOSER_DETAILS_VIEWBUTTON_TOOLTIP_TEXT("FileChooser.detailsViewButtonToolTipText"),
	FILECHOOSER_DETAILS_VIEWACTION_LABEL_TEXT("FileChooser.detailsViewActionLabelText"),
	FILECHOOSER_DETAILS_VIEW_BUTTONACCESSIBLE_NAME("FileChooser.detailsViewButtonAccessibleName"),
	FILECHOOSER_LIST_VIEW_BUTTON_TOOLTIP_TEXT("FileChooser.listViewButtonToolTipText"),
	FILECHOOSER_LIST_VIEW_ACTIONLABEL_TEXT("FileChooser.listViewActionLabelText"),
	FILECHOOSER_LIST_VIEW_BUTTONACCESSIBLE_NAME("FileChooser.listViewButtonAccessibleName"),
	FILECHOOSER_LOOK_INLABEL_TEXT("FileChooser.lookInLabelText"),
	FILECHOOSER_FILE_SOFTYPE_LABEL_TEXT("FileChooser.filesOfTypeLabelText"),
	FILECHOOSER_NEWFOLDER_ACCESSIBLE_NAME("FileChooser.newFolderAccessibleName"),
	FILECHOOSER_NEWFOLDER_ACTION_LABEL_TEXT("FileChooser.newFolderActionLabelText"),
	FILECHOOSER_REFRESH_ACTION_LABEL_TEXT("FileChooser.refreshActionLabelText"),
	FILECHOOSER_VIEW_MENU_LABEL_TEXT("FileChooser.viewMenuLabelText"),

	FILECHOOSER_FILENAME_HEADER_TEXT("FileChooser.fileNameHeaderText"),
	FILECHOOSER_FILESIZE_HEADER_TEXT("FileChooser.fileSizeHeaderText"),
	FILECHOOSER_FILETYPE_HEADER_TEXT("FileChooser.fileTypeHeaderText"),
	FILECHOOSER_FILEDATE_HEADER_TEXT("FileChooser.fileDateHeaderText"),
	FILECHOOSER_FILEATTR_HEADER_TEXT("FileChooser.fileAttrHeaderText"),

	FILECHOOSER_FOLDERNAME_LABEL_TEXT("FileChooser.folderNameLabelText");


	private final String key;

	EKeys (String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

}
