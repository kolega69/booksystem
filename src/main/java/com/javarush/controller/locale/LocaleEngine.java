package main.java.com.javarush.controller.locale;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Created by Oleg on 03.03.14.
 */
public class LocaleEngine {

	public static final String LOCALIZATION_KEY = "locKey";

	private String resourceBundleName;
	private ResourceBundle bundle;

	static Map<Class, ComponentHandler> handlers = new HashMap<>();

	static {
		new ComponentHandler(JLabel.class) {
			@Override
			public void apply(JComponent c, String value) {
				((JLabel) c).setText(value);
			}
		};
		new ComponentHandler(JButton.class) {
			@Override
			public void apply(JComponent c, String value) {
				((JButton) c).setText(value);
				c.setToolTipText(value);
			}
		};
		new ComponentHandler(JMenu.class) {
			@Override
			public void apply(JComponent c, String value) {
				((JMenu) c).setText(value);
			}
		};
		new ComponentHandler(JMenuItem.class) {
			@Override
			public void apply(JComponent c, String value) {
				((JMenuItem) c).setText(value);
			}
		};
		new ComponentHandler(JRadioButtonMenuItem.class) {
			@Override
			public void apply(JComponent c, String value) {
				((JRadioButtonMenuItem) c).setText(value);
			}
		};
	}

	public LocaleEngine(String resourceBundleName) {
		this.resourceBundleName = resourceBundleName;
	}

	public void setLocale(String newLocale) {
		bundle = ResourceBundle.getBundle(resourceBundleName, new Locale(newLocale));
	}

	public void changeLocale(Container topLevelContainer) {
		processContainer(topLevelContainer);
	}

	public void changeLocale(JMenuBar menuBar) {
		for (Component c : menuBar.getComponents()) {
			if (c instanceof JMenu) {
				processMenuItem((JMenu) c);
			} else if (c instanceof JComponent) {
				processJComponent((JComponent) c);
			}
		}
	}

	public void changeLocale(JPopupMenu popupMenu) {
		for (Component c : popupMenu.getComponents()) {
			if (c instanceof JMenu) {
				processMenuItem((JMenu) c);
			} else if (c instanceof JComponent) {
				processJComponent((JComponent) c);
			}
		}
	}

	private void processMenuItem(JMenuItem mi) {
		if (mi == null) return;
		Object cp = mi.getClientProperty(LOCALIZATION_KEY);
		if (cp != null) {
			applyLocale(mi, (String) cp);
		}
		if (mi instanceof JMenu) {
			JMenu m = (JMenu) mi;
			for (int i = 0; i < m.getItemCount(); i++) {
				processMenuItem(m.getItem(i));
			}
		}
	}

	private void processContainer(Container container) {
		for (Component c : container.getComponents()) {
			if (c instanceof Container) {
				processContainer((Container) c);
				if (c instanceof JComponent) {
					processJComponent((JComponent) c);
				}
			}
		}
	}

	private void processJComponent(JComponent jc) {
		Object cp = jc.getClientProperty(LOCALIZATION_KEY);
		if (cp != null) {
			applyLocale(jc, (String) cp);
			jc.setToolTipText(getStringResource((String) cp));
		}
	}

	private void applyLocale(JComponent c, String key) {
		ComponentHandler ch = handlers.get(c.getClass());
		if (ch == null) {
			return;
		}
		ch.apply(c, bundle.getString(key));
	}

	public String getStringResource(String key) {
		return bundle.getString(key);
	}

	public void localizeOptionPane() {
		UIManager.put(EKeys.OPTIONPANE_YES_BUTTON_TEXT.getKey(),
				getStringResource(EKeys.OPTIONPANE_YES_BUTTON_TEXT.getKey()));
		UIManager.put(EKeys.OPTIONPANE_CANCEL_BUTTON_TEXT.getKey(),
				getStringResource(EKeys.OPTIONPANE_CANCEL_BUTTON_TEXT.getKey()));
		UIManager.put(EKeys.OPTIONPANE_NO_BUTTON_TEXT.getKey(),
				getStringResource(EKeys.OPTIONPANE_NO_BUTTON_TEXT.getKey()));
		UIManager.put(EKeys.OPTIONPANE_TITLE_TEXT.getKey(),
				getStringResource(EKeys.OPTIONPANE_TITLE_TEXT.getKey()));

	}

	public void localizeFileChooser() {
		UIManager.put(EKeys.FILECHOOSER_CANCEL_BUTTON_TEXT.getKey(),
                getStringResource(EKeys.FILECHOOSER_CANCEL_BUTTON_TEXT.getKey()));
        UIManager.put(EKeys.FILECHOOSER_ACCEPT_ALLFILE_FILTER_TEXT.getKey(),
                getStringResource(EKeys.FILECHOOSER_ACCEPT_ALLFILE_FILTER_TEXT.getKey()));
        UIManager.put(EKeys.FILECHOOSER_CANCEL_BUTTON_MNEMONIC.getKey(),
                getStringResource(EKeys.FILECHOOSER_CANCEL_BUTTON_MNEMONIC.getKey()));
        UIManager.put(EKeys.FILECHOOSER_CANCEL_BUTTON_TOOLTIP_TEXT.getKey(),
                getStringResource(EKeys.FILECHOOSER_CANCEL_BUTTON_TOOLTIP_TEXT.getKey()));
        UIManager.put(EKeys.FILECHOOSER_DIRECTORY_DESCRIPTION_TEXT.getKey(),
                getStringResource(EKeys.FILECHOOSER_DIRECTORY_DESCRIPTION_TEXT.getKey()));
        UIManager.put(EKeys.FILECHOOSER_DIRECTORY_OPEN_BUTTON_TEXT.getKey(),
                getStringResource(EKeys.FILECHOOSER_DIRECTORY_OPEN_BUTTON_TEXT.getKey()));
        UIManager.put(EKeys.FILECHOOSER_DIRECTORY_OPEN_BUTTON_MNEMONIC.getKey(),
                getStringResource(EKeys.FILECHOOSER_DIRECTORY_OPEN_BUTTON_TEXT.getKey()));
        UIManager.put(EKeys.FILECHOOSER_DIRECTORY_OPEN_BUTTON_TOOLTIP_TEXT.getKey(),
                getStringResource(EKeys.FILECHOOSER_DIRECTORY_OPEN_BUTTON_TOOLTIP_TEXT.getKey()));
        UIManager.put(EKeys.FILECHOOSER_FILE_DESCRIPTION_TEXT.getKey(),
                getStringResource(EKeys.FILECHOOSER_FILE_DESCRIPTION_TEXT.getKey()));
        UIManager.put(EKeys.FILECHOOSER_FILE_SIZE_GIGABYTES.getKey(),
                getStringResource(EKeys.FILECHOOSER_FILE_SIZE_GIGABYTES.getKey()));
        UIManager.put(EKeys.FILECHOOSER_FILESIZE_KILOBYTES.getKey(),
                getStringResource(EKeys.FILECHOOSER_FILESIZE_KILOBYTES.getKey()));
        UIManager.put(EKeys.FILECHOOSER_FILESIZE_MEGA_BYTES.getKey(),
                getStringResource(EKeys.FILECHOOSER_FILESIZE_MEGA_BYTES.getKey()));
        UIManager.put(EKeys.FILECHOOSER_HELP_BUTTON_MNEMONIC.getKey(),
                getStringResource(EKeys.FILECHOOSER_HELP_BUTTON_MNEMONIC.getKey()));
        UIManager.put(EKeys.FILECHOOSER_HELP_BUTTON_TEXT.getKey(),
                getStringResource(EKeys.FILECHOOSER_HELP_BUTTON_TEXT.getKey()));
        UIManager.put(EKeys.FILECHOOSER_HELP_BUTTON_TOOLTIP_TEXT.getKey(),
                getStringResource(EKeys.FILECHOOSER_HELP_BUTTON_TOOLTIP_TEXT.getKey()));
        UIManager.put(EKeys.FILECHOOSER_NEWFOLDER_ERROR_SEPARATOR.getKey(),
                getStringResource(EKeys.FILECHOOSER_NEWFOLDER_ERROR_SEPARATOR.getKey()));
        UIManager.put(EKeys.FILECHOOSER_NEWFOLDER_ERROR_TEXT.getKey(),
                getStringResource(EKeys.FILECHOOSER_NEWFOLDER_ERROR_TEXT.getKey()));
        UIManager.put(EKeys.FILECHOOSER_OPEN_BUTTON_MNEMONIC.getKey(),
                getStringResource(EKeys.FILECHOOSER_OPEN_BUTTON_MNEMONIC.getKey()));
        UIManager.put(EKeys.FILECHOOSER_OPEN_BUTTON_TOOLTIP_TEXT.getKey(),
                getStringResource(EKeys.FILECHOOSER_OPEN_BUTTON_TEXT.getKey()));
        UIManager.put(EKeys.FILECHOOSER_OPEN_DIALOG_TITLE_TEXT.getKey(),
                getStringResource(EKeys.FILECHOOSER_OPEN_DIALOG_TITLE_TEXT.getKey()));
        UIManager.put(EKeys.FILECHOOSER_OTHER_NEWFOLDER.getKey(),
                getStringResource(EKeys.FILECHOOSER_OTHER_NEWFOLDER.getKey()));
        UIManager.put(EKeys.FILECHOOSER_OTHER_NEWFOLDER_SUBSEQUENT.getKey(),
                getStringResource(EKeys.FILECHOOSER_OTHER_NEWFOLDER_SUBSEQUENT.getKey()));
        UIManager.put(EKeys.FILECHOOSER_SAVE_BUTTON_MNEMONIC.getKey(),
                getStringResource(EKeys.FILECHOOSER_SAVE_BUTTON_MNEMONIC.getKey()));
        UIManager.put(EKeys.FILECHOOSER_SAVE_BUTTON_TEXT.getKey(),
                getStringResource(EKeys.FILECHOOSER_SAVE_BUTTON_TEXT.getKey()));
        UIManager.put(EKeys.FILECHOOSER_SAVE_BUTTON_TOOLTIP_TEXT.getKey(),
                getStringResource(EKeys.FILECHOOSER_SAVE_BUTTON_TOOLTIP_TEXT.getKey()));
        UIManager.put(EKeys.FILECHOOSER_SAVE_DIALOG_TITLE_TEXT.getKey(),
                getStringResource(EKeys.FILECHOOSER_SAVE_DIALOG_TITLE_TEXT.getKey()));
        UIManager.put(EKeys.FILECHOOSER_UPDATE_BUTTON_MNEMONIC.getKey(),
                getStringResource(EKeys.FILECHOOSER_UPDATE_BUTTON_MNEMONIC.getKey()));
        UIManager.put(EKeys.FILECHOOSER_UPDATE_BUTTON_TEXT.getKey(),
                getStringResource(EKeys.FILECHOOSER_UPDATE_BUTTON_TEXT.getKey()));
        UIManager.put(EKeys.FILECHOOSER_UPDATE_BUTTON_TOOLTIP_TEXT.getKey(),
                getStringResource(EKeys.FILECHOOSER_UPDATE_BUTTON_TOOLTIP_TEXT.getKey()));
        UIManager.put(EKeys.FILECHOOSER_WIN32_NEWFOLDER.getKey(),
                getStringResource(EKeys.FILECHOOSER_WIN32_NEWFOLDER.getKey()));
        UIManager.put(EKeys.FILECHOOSER_WIN32_NEWFOLDER_SUBSEQUENT.getKey(),
                getStringResource(EKeys.FILECHOOSER_WIN32_NEWFOLDER_SUBSEQUENT.getKey()));
		UIManager.put(EKeys.FILECHOOSER_HOME_FOLDER_ACCESSIBLE_NAME.getKey(),
				getStringResource(EKeys.FILECHOOSER_HOME_FOLDER_ACCESSIBLE_NAME.getKey()));
		UIManager.put(EKeys.FILECHOOSER_HOME_FOLDER_TOOLTIP_TEXT.getKey(),
				getStringResource(EKeys.FILECHOOSER_HOME_FOLDER_TOOLTIP_TEXT.getKey()));
		UIManager.put(EKeys.FILECHOOSER_NEW_FOLDER_TOOLTIPTEXT.getKey(),
				getStringResource(EKeys.FILECHOOSER_NEW_FOLDER_TOOLTIPTEXT.getKey()));
		UIManager.put(EKeys.FILECHOOSER_UPFOLDER_TOOLTIP_TEXT.getKey(),
				getStringResource(EKeys.FILECHOOSER_UPFOLDER_TOOLTIP_TEXT.getKey()));
		UIManager.put(EKeys.FILECHOOSER_DETAILS_VIEWBUTTON_TOOLTIP_TEXT.getKey(),
				getStringResource(EKeys.FILECHOOSER_DETAILS_VIEWBUTTON_TOOLTIP_TEXT.getKey()));
		UIManager.put(EKeys.FILECHOOSER_DETAILS_VIEWACTION_LABEL_TEXT.getKey(),
				getStringResource(EKeys.FILECHOOSER_DETAILS_VIEWACTION_LABEL_TEXT.getKey()));
		UIManager.put(EKeys.FILECHOOSER_DETAILS_VIEW_BUTTONACCESSIBLE_NAME.getKey(),
				getStringResource(EKeys.FILECHOOSER_DETAILS_VIEW_BUTTONACCESSIBLE_NAME.getKey()));
		UIManager.put(EKeys.FILECHOOSER_LIST_VIEW_BUTTON_TOOLTIP_TEXT.getKey(),
				getStringResource(EKeys.FILECHOOSER_LIST_VIEW_BUTTON_TOOLTIP_TEXT.getKey()));
		UIManager.put(EKeys.FILECHOOSER_DETAILS_VIEWACTION_LABEL_TEXT.getKey(),
				getStringResource(EKeys.FILECHOOSER_DETAILS_VIEWACTION_LABEL_TEXT.getKey()));
		UIManager.put(EKeys.FILECHOOSER_LIST_VIEW_ACTIONLABEL_TEXT.getKey(),
				getStringResource(EKeys.FILECHOOSER_LIST_VIEW_ACTIONLABEL_TEXT.getKey()));
		UIManager.put(EKeys.FILECHOOSER_LIST_VIEW_BUTTONACCESSIBLE_NAME.getKey(),
				getStringResource(EKeys.FILECHOOSER_LIST_VIEW_BUTTONACCESSIBLE_NAME.getKey()));
		UIManager.put(EKeys.FILECHOOSER_LOOK_INLABEL_TEXT.getKey(),
				getStringResource(EKeys.FILECHOOSER_LOOK_INLABEL_TEXT.getKey()));
		UIManager.put(EKeys.FILECHOOSER_FILE_SOFTYPE_LABEL_TEXT.getKey(),
				getStringResource(EKeys.FILECHOOSER_FILE_SOFTYPE_LABEL_TEXT.getKey()));
		UIManager.put(EKeys.FILECHOOSER_NEWFOLDER_ACCESSIBLE_NAME.getKey(),
				getStringResource(EKeys.FILECHOOSER_NEWFOLDER_ACCESSIBLE_NAME.getKey()));
		UIManager.put(EKeys.FILECHOOSER_NEWFOLDER_ACTION_LABEL_TEXT.getKey(),
				getStringResource(EKeys.FILECHOOSER_NEWFOLDER_ACTION_LABEL_TEXT.getKey()));
		UIManager.put(EKeys.FILECHOOSER_REFRESH_ACTION_LABEL_TEXT.getKey(),
				getStringResource(EKeys.FILECHOOSER_REFRESH_ACTION_LABEL_TEXT.getKey()));
		UIManager.put(EKeys.FILECHOOSER_VIEW_MENU_LABEL_TEXT.getKey(),
				getStringResource(EKeys.FILECHOOSER_VIEW_MENU_LABEL_TEXT.getKey()));

		UIManager.put(EKeys.FILECHOOSER_FILENAME_HEADER_TEXT.getKey(),
				getStringResource(EKeys.FILECHOOSER_FILENAME_HEADER_TEXT.getKey()));
		UIManager.put(EKeys.FILECHOOSER_FILESIZE_HEADER_TEXT.getKey(),
				getStringResource(EKeys.FILECHOOSER_FILESIZE_HEADER_TEXT.getKey()));
		UIManager.put(EKeys.FILECHOOSER_FILETYPE_HEADER_TEXT.getKey(),
				getStringResource(EKeys.FILECHOOSER_FILETYPE_HEADER_TEXT.getKey()));
		UIManager.put(EKeys.FILECHOOSER_FILEDATE_HEADER_TEXT.getKey(),
				getStringResource(EKeys.FILECHOOSER_FILEDATE_HEADER_TEXT.getKey()));
		UIManager.put(EKeys.FILECHOOSER_FILEATTR_HEADER_TEXT.getKey(),
				getStringResource(EKeys.FILECHOOSER_FILEATTR_HEADER_TEXT.getKey()));
		UIManager.put(EKeys.FILECHOOSER_FOLDERNAME_LABEL_TEXT.getKey(),
				getStringResource(EKeys.FILECHOOSER_FOLDERNAME_LABEL_TEXT.getKey()));

	}


	abstract static class ComponentHandler {

		private ComponentHandler(Class cls) {
			handlers.put(cls, this);
		}

		public abstract void apply(JComponent c, String value);
	}

}
