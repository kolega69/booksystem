package main.java.com.javarush.controller;

import main.java.com.javarush.exceptions.BookSystemException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;


public class Property {
    public String paths;
    public String exceptPath;
    public String booksPerPage;
    public String rootDir;
    public List<String> bookExtensions;
    public String fileExtensions;
	private String currentLanguage;
	private List<String> languagesBunch;
	private String defaultLanguage;
    private final Properties properties;
	public final static String INDEX_DIR = System.getProperty("user.home") + "/.booksystem/index/";

    private static Property instance;

    public static Property getInstance() throws BookSystemException {
        if (instance == null) {
            instance = new Property();
        }
        return instance;
    }

    private Property() throws BookSystemException {
        properties = new Properties();

        try {
            properties.load(new FileInputStream(new File("config/config.properties")));
        } catch (IOException e) {
            throw new BookSystemException("ERROR! \n" +
                    "There are some problems with config");
        }

	    bookExtensions = new ArrayList<>();
	    languagesBunch = new ArrayList<>();

        paths = properties.getProperty("paths");
        exceptPath = properties.getProperty("exceptPath");
        booksPerPage = properties.getProperty("booksPerPage");
        rootDir = properties.getProperty("rootDir");
        fileExtensions = properties.getProperty("fileExtensions");
	    currentLanguage = properties.getProperty("current_language");
	    defaultLanguage = properties.getProperty("lang.default");
	    String languages = properties.getProperty("languages");
	    splitAndFill(fileExtensions, bookExtensions);
	    splitAndFill(languages, languagesBunch);

    }

    public void saveProperty() throws BookSystemException {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(new File("config/config.properties"));
            properties.setProperty("paths", paths);
            properties.setProperty("exceptPath", exceptPath);
            properties.setProperty("booksPerPage", booksPerPage);
            properties.setProperty("rootDir", rootDir);
            properties.setProperty("fileExtensions", fileExtensions);
	        properties.setProperty("current_language", currentLanguage);
            properties.store(fileOutputStream, null);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            throw new BookSystemException("ERROR! \n" +
                    "Can write new config");
        }
    }

	public void setCurrentLanguage(String currentLanguage) {
		this.currentLanguage = currentLanguage;
	}

	public String getCurrentLanguage() {
		return currentLanguage;
	}

	public List<String> getLanguagesBunch() {
		return languagesBunch;
	}

	private void splitAndFill(String s, List<String> masS) {
		if (s.isEmpty()) return;
		String[] elements = s.split(",");
		Collections.addAll(masS, elements);
	}

}
