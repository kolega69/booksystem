package main.java.com.javarush.controller;

import main.java.com.javarush.controller.locale.EKeys;
import main.java.com.javarush.controller.locale.LocaleEngine;
import main.java.com.javarush.exceptions.BookSystemException;
import main.java.com.javarush.model.Constants;
import main.java.com.javarush.model.Model;
import main.java.com.javarush.model.Paging;
import main.java.com.javarush.service.BookDataProvider;
import main.java.com.javarush.service.LuceneSearcher;
import main.java.com.javarush.view.BookSystemAbout;
import main.java.com.javarush.view.BookSystemView;
import main.java.com.javarush.view.book.BookSettings;
import main.java.com.javarush.view.component.table.BookSystemTable;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainController {
    private Model model;
    private Paging paging;
    private LuceneSearcher luceneSearcher;
    private BookSystemView view;
    private BookSettings bookSettingsView;
    private BookSystemAbout bookSystemAbout;
    private BookDataProvider bookDataProvider = new BookDataProvider();
	private String fileName;
    private boolean isFavoriteTab = false;
    private int currentPage;
	private LocaleEngine localeEngine;
	private final String LOCALE_NAME = "main.java.com.javarush.controller.locale.language";

    public MainController() {
        this.localeEngine = new LocaleEngine(LOCALE_NAME);
        this.view = new BookSystemView(this);
        this.paging = new Paging(this);
    }

	public void applyLocale(String locale){
		localeEngine.setLocale(locale);
		view.applyLocale(localeEngine);
	}


	public void setModel(Model model) {
        this.model = model;
    }

    public Model getModel() {
        return model;
    }

    public BookSystemView getView() {
        return view;
    }

    public void setLuceneSearcher(LuceneSearcher luceneSearcher) {
        this.luceneSearcher = luceneSearcher;
    }

    public void showMainWindow() {
        try {
            view.init();
        } catch (BookSystemException e) {
            view.showException(e);
        }
        view.show();
    }

    public void showExceptionToUser(BookSystemException e) {
        view.showException(e);
    }

    public void setIsFavoriteTab(boolean isFavoriteTab) {
        this.isFavoriteTab = isFavoriteTab;
    }

    public boolean getIsFavoriteTab() {
        return isFavoriteTab;
    }

    public void processIndex(File file, File newFile) throws BookSystemException {
        try {
            if(luceneSearcher.isIndexExist()) {
	            if (booksFromFavorites().contains(file)) {
		            luceneSearcher.updateIndex(newFile, Constants.YES.toString());
	            } else {
		            luceneSearcher.updateIndex(newFile, Constants.NO.toString());
	            }
            } else {  luceneSearcher.createIndex(newFile); }

	        luceneSearcher.closeIndexWriter();
	        luceneSearcher.closeIndexReader();
	        luceneSearcher.closeIndexSearcher();

        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public boolean hasFileSpecifiedBookExtension(String name) throws BookSystemException {
        List<String> extensions = model.getProperty().bookExtensions;
        String[] nameParts = name.split("\\.");
        if (nameParts.length > 1) {
            if (extensions.contains(nameParts[nameParts.length - 1].toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public void onAddNodeCategoriesMenuAction() {
        view.addNewCategoryNode();
    }

    public void onDeleteNodeCategoriesMenuAction() {
        try {
            view.deleteSelectedCategoryNode();
        } catch (BookSystemException e) {
            view.showException(e);
        }
    }

    public void onRenameNodeCategoriesMenuAction() {
        view.renameSelectedCategory();
    }

    public void moveFiles() {
        //TODO not implemented yet
    }

    public void scanBooks(String userPath) {
        try {
            isFavoriteTab = false;
            Property property = model.getProperty();
            String actualPath = userPath == null ? property.paths : userPath;
            List<File> tableBooks = model.getTableBooks();
            if (!tableBooks.isEmpty()) {
                tableBooks.clear();
            }
            try {
                bookDataProvider.scanFileSystem(actualPath, property.bookExtensions, property.exceptPath, property.rootDir);
            } catch (BookSystemException e) {
                view.showException(e);
            }
            view.getTabbedPane().setSelectedIndex(0);
            model.setTableBooks(bookDataProvider.getScannedBooks());
            paging.refresh(property.booksPerPage, bookDataProvider.getScannedBooks());
            view.refresh(paging.firstPage());
        } catch (BookSystemException e) {
            view.showException(e);
        }
    }

    public void showSettingsMenu() {
        try {
            if (bookSettingsView == null) {
                bookSettingsView = new BookSettings(view.getMainFrame(), "Settings", true, this);
            }
            bookSettingsView.applyLocale(localeEngine);

            bookSettingsView.setVisible(true);
        } catch (BookSystemException e) {
            view.showException(e);
        }
    }

    public void goToNextPage() {
        view.refresh(paging.pageUp());
    }

    public void goToPrevPage() {
        view.refresh(paging.pageDown());
    }

    public void renameSelectedFile() {
        view.renameSelectedFile();
    }

    /**
     * Searching books with the name taken from textField.
     */
    public void searchBooksInsideTable() {
        try {
            String bookName = model.getSearchQueryText();

            if (bookName != null && !bookName.isEmpty()) {
                luceneSearcher.searchIndex(bookName, model.getTableBooks());
            }

            paging.refresh(model.getProperty().booksPerPage, model.getTableBooks());
            view.refresh(paging.firstPage());
        } catch (BookSystemException e) {
            view.showException(e);
        }
    }

    public Path getRootDirParent() throws BookSystemException {
        String rootDir = model.getProperty().rootDir;
        Path rootDirP;
        if (Paths.get(rootDir).getNameCount() == 0)
            rootDirP = Paths.get(rootDir);
        else
            rootDirP = Paths.get(rootDir).getParent();
        return rootDirP;
    }

    public void exitApp() {
        System.exit(0);
    }

    public List<File> getActualOnePageBooksFromPaging() {
        List<File> tableBooks = model.getTableBooks();
        if (tableBooks.isEmpty()) {
            return Collections.emptyList();
        }

        paging.refresh(tableBooks);
        paging.changeCurrentPage();
        paging.selectBooks();

        return paging.getOnePage();
    }

    public void deleteBooksFromTableAfterDragAndDrop(ArrayList<File> toBeRemovedBooks) {
        model.getTableBooks().removeAll(toBeRemovedBooks);
    }

    public File renameFile(Object newName, int rowIndex) {
        int number = rowIndex + paging.getCountOnPrevPages();
        List<File> tableBooks;
        List<File> tableBooks1 = model.getTableBooks();
        if (isFavoriteTab) {
            tableBooks = luceneSearcher.getFavoriteBooks();
        } else {
            tableBooks = model.getTableBooks();
        }
        File file = tableBooks.get(number);
        int number1 = tableBooks1.indexOf(file);
        String oldName = file.getName();
        String extension = oldName.substring(oldName.lastIndexOf('.'));
        String folder = file.getParent() + "/";
        StringBuilder builder = new StringBuilder();
        if (!newName.toString().equals(oldName)) {
            if (!newName.equals("")) {
                if (!newName.toString().matches("^.*[*?/\\\\<>:|" + '"' + "].*$")) {
                    builder.append(folder).append(newName.toString()).append(extension);
                    File newFile = new File(builder.toString());
                    if (file.renameTo(newFile)) {
                        try {
                            if (luceneSearcher.isContainBook(oldName)){
                                String option = luceneSearcher.bookIsFavorite(oldName) ? Constants.YES.toString() :
                                        Constants.NO.toString();
                                luceneSearcher.removeIndex(luceneSearcher.getId(oldName));
                                luceneSearcher.updateIndex(newFile, option);
                                luceneSearcher.closeIndexWriter();
                                if (isFavoriteTab && number1 > -1) tableBooks1.set(number1, newFile);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        tableBooks.set(number, newFile);
                        return newFile;
                    }
                } else {
                    view.showException(new BookSystemException
                                      (localeEngine.getStringResource(EKeys.INCORRECT_FILE_NAME.getKey())));
                }
            } else {
                view.showException(new BookSystemException
                                  (localeEngine.getStringResource(EKeys.CAN_NOT_SET_NAME.getKey())));
            }
        }
        return file;
    }

    public int getRowCountOnOnePage() {
        if(!isFavoriteTab) {
            if (model.getTableBooks().isEmpty()) {
                return 0;
            } else {
                return paging.getRowCountOnOnePage();
            }
        }
        else {
            if(model.getFavoriteBooks().isEmpty()) {
                return 0;
            }
            else {
                return paging.getRowCountOnOnePage();
            }
        }
    }

    public void switchButtons(int currentPage, int pageCount) {
        if (pageCount > 1) {
            if (currentPage > 1 && currentPage < pageCount) {
                view.disableEnableNavigationButtons(true, true);
            } else
                if (currentPage == pageCount) {
                    view.disableEnableNavigationButtons(false, true);
                } else {
                    view.disableEnableNavigationButtons(true, false);
                }
        } else {
            view.disableEnableNavigationButtons(false, false);
        }
        view.refreshPagerUIData(currentPage, pageCount);
    }

    public void openBookWithProperApplication(int n) {
        int index = n + paging.getCountOnPrevPages();
            if(!isFavoriteTab) {
                openFileWithNativeOS(model.getTableBooks().get(index));
            }
            else {
                openFileWithNativeOS(model.getFavoriteBooks().get(index));
            }
    }

	public void showHelp() {
		File help = new File("help/manual.chm");
		if (help.exists()){
			openFileWithNativeOS(help);
		} else {
			view.showException(new BookSystemException
					(localeEngine.getStringResource(EKeys.HELP_IS_ABSENT.getKey())));
		}
	}

    public void openFileWithNativeOS(Object obj) {
        String operationSystem = System.getProperty("os.name").toLowerCase();
        String cmd = null;
        if (obj instanceof File) {
            File book = (File) obj;
            cmd = book.getPath();
        } else if (obj instanceof String){
            cmd = (String) obj;
        }
        try {
            if(operationSystem.contains("windows")) {
                ProcessBuilder pb = null;
                if (obj instanceof File){
                    pb = new ProcessBuilder("cmd.exe", "/c", cmd);
                }
                else if (obj instanceof String){
                    pb = new ProcessBuilder("rundll32", "url.dll,FileProtocolHandler", cmd);
                }
                Process p = pb.start();
            }
            else if(operationSystem.contains("linux")) {
                ProcessBuilder pb = new ProcessBuilder("xdg-open", cmd);
                Process p = pb.start();
            }
        } catch(IOException e) {
            view.showException(new BookSystemException
                    (localeEngine.getStringResource(EKeys.NOT_SUPPORT_FORMAT.getKey())));
        }
    }

    public void showTablePopupMenu(Component component, int x, int y) {
	    selectTableMenuItems();
        view.showTablePopupMenu(component, x, y);
    }

	public void selectTableMenuItems() {
        if(!isFavoriteTab) {
            BookSystemTable tableBooks = view.getTableBooks();
            int row = tableBooks.getSelectedRow() + paging.getCountOnPrevPages();
            File file = model.getTableBooks().get(row);

            boolean bookExists;
            fileName = file.getName();
            try {
                bookExists = luceneSearcher.isContainBook(fileName);
                if (bookExists) {
                    if (luceneSearcher.bookIsFavorite(fileName)) {
                        view.addRemoveFromIndexMenuItem();
                        view.removeMenuItemAddToFavorites();
                    }  else {
                        view.addToFavoritesMenuItem();
                        view.removeMenuItemRemoveFromFavorites();
                    }
                } else {
                    view.removeMenuItemRemoveFromFavorites();
                    view.removeMenuItemAddToFavorites();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(isFavoriteTab) {
            BookSystemTable favoriteBooks = view.getFavoriteBooks();
            int row = favoriteBooks.getSelectedRow() + paging.getCountOnPrevPages();
            File file = model.getFavoriteBooks().get(row);
            fileName = file.getName();
            view.addRemoveFromIndexMenuItem();
            view.removeMenuItemAddToFavorites();
        }
	}

    public void showCategoriesPopupMenu(Component component, int x, int y) {
        view.showCategoriesPopupMenu(component, x, y);
    }

    public boolean isDirectoryNameValid(String nameDirectory) {
        if (nameDirectory.equals("") ||
                nameDirectory.matches("^.*[*?/\\\\<>:|" + '"' + "].*$") ||
                nameDirectory.equals(model.getLastCategoryOnRename()))
        {
            view.showException(new BookSystemException
                              (localeEngine.getStringResource(EKeys.INCORRECT_FOLDER_NAME.getKey())));
            try {
                view.refreshJTree();
            } catch (BookSystemException e) {
                e.printStackTrace();
            }
            return false;
        }

        return true;
    }

    public void createFileOnFileSystem(String strDirectory) {
        model.setTableEditable(false);

        if (model.isCategoryAdded()) {
            model.setCategoryAdded(false);
            boolean success = (new File(strDirectory)).mkdir();
            if (!success) {
                view.showException(
                        new BookSystemException(localeEngine.getStringResource
                                               (EKeys.DIRECTORY_CREATING_FILED.getKey())));
            }
        } else
            if (model.isCategoryRenamed()) {
                File file = new File(model.getLastCategoryOnRename());
                File newFile = new File(strDirectory);
                boolean success = file.renameTo(newFile);
                if (!success) {
                    view.showException(new BookSystemException(localeEngine.getStringResource
                                                              (EKeys.RENAMING_FILED.getKey())));
                    return;
                }
                model.setLastCategoryOnRename(null);
	            model.setCategoryRenamed(false);
            }
    }

    public void refreshJTreeView() throws BookSystemException {
        view.refreshJTree();
        luceneSearcher.clearIndex();
        luceneSearcher.updateRootIndex();
    }

	public void addFileToFavorites() {
		luceneSearcher.setBookAsFavorite(fileName);
	}

	public void removeFileFromFavorites() {
		luceneSearcher.removeBookFromFavorites(fileName);
        if(isFavoriteTab) {
            model.setFavoriteBooks(booksFromFavorites());
            paging.refresh(model.getFavoriteBooks());
            view.refresh(paging.firstPage());
        }
	}

    public List<File> booksFromFavorites(){
        return luceneSearcher.getFavoriteBooks();
    }

    public void showFavoriteBooks() {
        List<File> favorite = booksFromFavorites();
        if (!favorite.isEmpty()) {
            favorite.clear();
        }
        try {
            currentPage = paging.getCurrentPage();
            favorite = booksFromFavorites();
            model.setFavoriteBooks(favorite);
            paging.refresh(model.getProperty().booksPerPage, favorite);
            view.refresh(paging.firstPage());
        } catch (BookSystemException e) {
            e.printStackTrace();
        }
    }

    public void showBooks() {
        try {
            List<File> books = model.getTableBooks();
            if (books.isEmpty()) currentPage = 0;
            paging.refresh(model.getProperty().booksPerPage, books);
            if (currentPage > 0){
                paging.setCurrentPage(currentPage);
                view.refresh(paging.getOnePage());
            }
        } catch (BookSystemException e) {
            e.printStackTrace();
        }
    }

    public void checkIsFoldersExist() {
        Property property = null;
        boolean isExist = true;

        try {
            property = Property.getInstance();
        } catch(BookSystemException e) {}

        File storeDir = new File(property.rootDir);
        String path = property.paths;
        String[] filePaths = path.split(";");

        List<File> files = new ArrayList<>();
        for(String s : filePaths) {
            files.add(new File(s));
        }

        for(File f : files) {
            if(!f.exists()) {
                isExist = false;
                break;
            }
        }

        if(!storeDir.exists() || !isExist) {
            view.showMessage();
        }
    }

    public void showAbout() {
        if (bookSystemAbout == null) {
            bookSystemAbout = new BookSystemAbout(view.getMainFrame(), "About", true, this);
        }
        bookSystemAbout.applyLocale(localeEngine);
        bookSystemAbout.setVisible(true);
    }

	public int showOverwriteMassage(File newFile) {
		return view.showOverwriteDialog(newFile);
	}

	public String getChooseBtnText() {
		return localeEngine.getStringResource(EKeys.FILECHOOSER_CHOOSE_BUTTON_TEXT.getKey());
	}

	public String getDirNotExistText() {
		return localeEngine.getStringResource(EKeys.DIRECTORY_NOT_EXIST.getKey());
	}

	public String getSpecifyDirectoryText() {
		return localeEngine.getStringResource(EKeys.SPECIFY_DIRECTORY.getKey());
	}

    public String getSearchFieldText() {
        return localeEngine.getStringResource(EKeys.SEARCH_FIELD.getKey());
    }
    public String getTextAbout() {
        return localeEngine.getStringResource(EKeys.TEXT_ABOUT.getKey());
    }
}
