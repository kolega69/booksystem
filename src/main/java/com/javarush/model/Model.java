package main.java.com.javarush.model;

import main.java.com.javarush.controller.Property;
import main.java.com.javarush.exceptions.BookSystemException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Model {
    private List<File> tableBooks = new ArrayList<File>();
    private List<File> favoriteBooks = new ArrayList<File>();
    private String searchQueryText;
    private String lastCategoryOnRename;
    private boolean categoryAdded;
    private boolean categoryRenamed;
    private boolean tableEditable;

    public Property getProperty() throws BookSystemException {
        return Property.getInstance();
    }

    public List<File> getTableBooks() {
        return tableBooks;
    }

    public void setTableBooks(List<File> tableBooks) {
        this.tableBooks = tableBooks;
    }

    public List<File> getFavoriteBooks() {
        return favoriteBooks;
    }

    public void setFavoriteBooks(List<File> favoriteBooks) {
        this.favoriteBooks = favoriteBooks;
    }

    public void setSearchQueryText(String searchQueryText) {
        this.searchQueryText = searchQueryText;
    }

    public String getSearchQueryText() {
        return searchQueryText;
    }

    public void setLastCategoryOnRename(String lastCategoryOnRename) {
        this.lastCategoryOnRename = lastCategoryOnRename;
    }

    public String getLastCategoryOnRename() {
        return lastCategoryOnRename;
    }

    public void setCategoryAdded(boolean categoryAdded) {
        this.categoryAdded = categoryAdded;
    }

    public boolean isCategoryAdded() {
        return categoryAdded;
    }

    public void setCategoryRenamed(boolean categoryRenamed) {
        this.categoryRenamed = categoryRenamed;
    }

    public boolean isCategoryRenamed() {
        return categoryRenamed;
    }

    public boolean isTableEditable() {
        return tableEditable;
    }

    public void setTableEditable(boolean tableEditable) {
        this.tableEditable = tableEditable;
    }
}
