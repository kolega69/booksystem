package main.java.com.javarush.model;

import main.java.com.javarush.controller.MainController;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Paging {
    private int booksPerPage;
    private int pageCount = 0;
    private int ostatok;
    private int currentPage;
    private List<File> onePage;
    private List<File> listBooks;
    private MainController controller;

    public Paging(MainController controller) {
        this.controller = controller;
    }

    public void refresh(String booksPerPage_str, List<File> books) {
        booksPerPage = Integer.valueOf(booksPerPage_str);
        onePage = new ArrayList();
        listBooks = books;

        if(!listBooks.isEmpty()) {
            calculatePageCount();
        }
        else {
            pageCount = 0;
        }
    }

    public void refresh(List<File> books) {
        listBooks = books;
        calculatePageCount();
    }

    private void calculatePageCount() {
        ostatok = 0;
        currentPage = 1;

        if (listBooks.size() < booksPerPage) {
            pageCount = 1;
            ostatok = listBooks.size();
        } else
            if (listBooks.size() == booksPerPage) {
                pageCount = 1;
            } else
                if ((listBooks.size() > booksPerPage)
                        && (listBooks.size() % booksPerPage == 0))
                {
                    pageCount = listBooks.size() / booksPerPage;
                } else {
                    pageCount = listBooks.size() / booksPerPage + 1;
                    ostatok = listBooks.size() % booksPerPage;
                }
        controller.switchButtons(currentPage, pageCount);
    }

    public List<File> firstPage() {
        int realSize = pageCount > 1 ? booksPerPage : listBooks.size();
        onePage = listBooks.subList(0, realSize);
        controller.switchButtons(currentPage, pageCount);
        return onePage;
    }

    public List<File> pageUp() {
        if (currentPage < pageCount) {
            currentPage++;
        }
        selectBooks();
        return onePage;
    }

    public List<File> pageDown() {
        if (currentPage > 1) {
            currentPage--;
        }
        selectBooks();
        return onePage;
    }

    public void selectBooks() {
        int realSize = ostatok == 0 ? booksPerPage * currentPage : listBooks.size();
        onePage = listBooks.subList(booksPerPage * (currentPage - 1), realSize);
        controller.switchButtons(currentPage, pageCount);
    }

    public List<File> getOnePage() {
        return onePage;
    }

    public int getRowCountOnOnePage() {
        return ostatok <= 0 || currentPage < pageCount ? booksPerPage : ostatok;
    }

    public void changeCurrentPage() {
        int page = currentPage;
        if (page > pageCount) {
            currentPage = pageCount;
        }
    }

    public int getCountOnPrevPages() {
        return (currentPage - 1) * booksPerPage;
    }

    public int getCurrentPage(){
        return currentPage;
    }

    public void setCurrentPage(int currentPage){
        this.currentPage = (currentPage >= 1 && currentPage <= pageCount) ? currentPage : 1;
        selectBooks();
    }
}

