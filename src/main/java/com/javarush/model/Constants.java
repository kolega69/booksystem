package main.java.com.javarush.model;

/**
 * Created by Oleg on 13.02.14.
 */
public enum Constants {
	YES("yes"),
	NO("no");

	private final String value;

	private Constants(String value) {
		this.value = value;
	}


	@Override
	public String toString() {
		return value;
	}
}
