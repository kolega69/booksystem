package main.java.com.javarush.view.component.table;

import main.java.com.javarush.controller.MainController;
import main.java.com.javarush.listener.DragAndDropHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class BookSystemTable extends JTable {

    private final BookSystemTableModel tableModel;
    private final MainController controller;
	private final int width = 40;

    public BookSystemTable(final MainController controller) {
        this.controller = controller;
        this.tableModel = new BookSystemTableModel(controller);
        this.setModel(tableModel);

	    this.addMouseMotionListener(new MouseMotionListener() {
		    @Override
		    public void mouseDragged(MouseEvent e) {
		    }

		    @Override
		    public void mouseMoved(MouseEvent e) {
			    Point po = new Point(e.getX(), e.getY());
			    int r = rowAtPoint(po);

			    String name = (String)getValueAt(r, 1);

			    //if text is not fit table cell then will appears tooltip
			    double maxTextSize = new JLabel(name).getMaximumSize().getWidth();
			    double cellSize = getWidth() - width;
			    setToolTipText(null);
			    if (maxTextSize > cellSize) {
				    setToolTipText(name);
			    }

			    // selecting row beneath mouse
			    if (getSelectedRow() != r) {
				    clearSelection();
				    changeSelection(r, 1, true, false);
			    }
		    }
	    });
    }

    public void init() {
        setDragEnabled(true);
        addMouseListener(new MouseCommands(controller));
        setTransferHandler(new TransferHandler() {

            @Override
            public boolean canImport(TransferSupport info) {
                return false;
            }

            @Override
            public int getSourceActions(JComponent comp) {
                return COPY_OR_MOVE;
            }

            @Override
            public Transferable createTransferable(JComponent comp) {
                int[] rows = BookSystemTable.this.getSelectedRows();

                List files = new ArrayList<File>();
                for (int i = 0; i < rows.length; i++) {
                    if (rows[i] < 0 || rows[i] >= BookSystemTable.this.getRowCount()) return null;
                    File book = tableModel.getBookByRowIndex(rows[i]);
                    files.add(book);
                }
                return new TransferableFile(files);
            }

            @Override
            protected void exportDone(JComponent source, Transferable data, int action) {

	            if (action == COPY || action == NONE) return;
	            if (DragAndDropHandler.getWrittenFiles().isEmpty()) return;

	            ArrayList<File> files = new ArrayList<File>();
	            files.addAll(DragAndDropHandler.getWrittenFiles());
	            controller.deleteBooksFromTableAfterDragAndDrop(files);
	            tableModel.refreshData(controller.getActualOnePageBooksFromPaging());
            }
        });


        setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);

        getColumnModel().getColumn(0).setMinWidth(width);
        getColumnModel().getColumn(0).setMaxWidth(width);
        getColumnModel().getColumn(0).setPreferredWidth(width);
	    getColumnModel().getColumn(0).setResizable(false);
        setRowHeight(width);
        getColumnModel().getColumn(0).setCellRenderer(new JTableIconRenderer());
        getColumnModel().getColumn(1).setMinWidth(638);
        getColumnModel().getColumn(1).setMinWidth(638);
	    //todo: make cell renderer for center aline text
    }

    public void refreshData(List<File> files) {
        tableModel.refreshData(files);
    }

    public void renameSelectedFile() {
        controller.getModel().setTableEditable(true);

        String s = (String)getValueAt(getSelectedRow(), getSelectedColumn());

        JTextField tf = new JTextField(s);
        tf.setEditable(true);
        tf.setSelectedTextColor(Color.BLUE);

        getColumnModel().getColumn(getSelectedColumn()).setCellEditor(new DefaultCellEditor(tf));

        editCellAt(getSelectedRow(), getSelectedColumn());

        tf.requestFocus();
        tf.setSelectionStart(0);
        tf.setSelectionEnd(s.length());
    }

	private class TransferableFile implements Transferable {
        private List fileList;

        public TransferableFile(List files) {
            fileList = files;
        }

        // Returns an object which represents the data to be transferred.
        public Object getTransferData(DataFlavor flavor)
                throws UnsupportedFlavorException
        {

            if (flavor.equals(DataFlavor.javaFileListFlavor))
                return fileList;

            throw new UnsupportedFlavorException(flavor);
        }

        // Returns an array of DataFlavor objects indicating the flavors
        // the data can be provided in.
        public DataFlavor[] getTransferDataFlavors() {
            return new DataFlavor[]{DataFlavor.javaFileListFlavor};
        }

        // Returns whether or not the specified data flavor is supported for this object.
        public boolean isDataFlavorSupported(DataFlavor flavor) {
            return flavor.equals(DataFlavor.javaFileListFlavor);
        }

    }

//	class MyRenderer implements TableCellRenderer {
//		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
//			JLabel l = new JLabel();
//			if (value != null) {
//				l.setText(value.toString());
//			}
//
//			l.setHorizontalAlignment(JLabel.CENTER);
//			return l;
//		}
//
//	}


}
