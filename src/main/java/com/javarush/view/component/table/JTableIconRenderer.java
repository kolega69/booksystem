package main.java.com.javarush.view.component.table;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Serge
 * Date: 10.01.14
 * Time: 23:09
 */
public class JTableIconRenderer extends JLabel implements TableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column)
    {
        setVerticalAlignment(JLabel.CENTER);
        setHorizontalAlignment(JLabel.CENTER);
        if (value instanceof ImageIcon) {
            setText(null);
            setIcon((ImageIcon) value);
        } else {
            setIcon(null);
            setText(value.toString());
        }
        return this;
    }
}
