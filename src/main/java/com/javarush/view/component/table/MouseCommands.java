package main.java.com.javarush.view.component.table;

import main.java.com.javarush.controller.MainController;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by KOlegA on 09.01.14.
 */
public class MouseCommands implements MouseListener {

    private MainController controller;

    public MouseCommands(MainController controller) {
        this.controller = controller;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        JTable table = (JTable) e.getSource();
        if (SwingUtilities.isRightMouseButton(e)) {
            int row = table.rowAtPoint(e.getPoint());
            int column = table.columnAtPoint(e.getPoint());
            if (row >= 0 && column >= 1) {
                if (table.isEditing() && !table.getCellEditor().stopCellEditing()) {
                    return;
                }
                table.changeSelection(row, column, false, false);
            }
            showPopupMenu(e);
        } else
            if (SwingUtilities.isLeftMouseButton(e)) {
                if (e.getClickCount() == 2 && !table.isEditing()) {
                    controller.openBookWithProperApplication(table.rowAtPoint(e.getPoint()));
                }
            }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        JTable table = (JTable) e.getSource();
        if (table.columnAtPoint(e.getPoint()) != 0)
            showPopupMenu(e);
    }

    private void showPopupMenu(MouseEvent e) {
        if (e.isPopupTrigger()) {
            controller.showTablePopupMenu(e.getComponent(), e.getX(), e.getY());
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }
}
