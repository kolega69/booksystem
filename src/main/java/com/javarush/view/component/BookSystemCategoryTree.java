package main.java.com.javarush.view.component;

import main.java.com.javarush.controller.MainController;
import main.java.com.javarush.exceptions.BookSystemException;
import main.java.com.javarush.listener.CategoriesTreeModelListener;
import main.java.com.javarush.listener.DragAndDropHandler;
import main.java.com.javarush.listener.JTreeMouseListener;

import javax.swing.*;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: dj
 * Date: 12.01.14
 * Time: 0:15
 */
public class BookSystemCategoryTree {
    private MainController controller;

    private DefaultMutableTreeNode m_rootNode;
    private DefaultTreeModel m_model;
    private JTree jTree;
    private JScrollPane jTreeScrollPanel;
	private JFrame mainFrame;
    private JPanel jPanel;

    public BookSystemCategoryTree(MainController controller, JFrame frame) throws BookSystemException {
        this.controller = controller;
	    this.mainFrame = frame;
    }

    public void init() throws BookSystemException {
        initTreeModel();
        jTree = new JTree(m_model);
        jTree.setRootVisible(false);
        jTree.setPreferredSize(new Dimension(200, 800));
        jTree.setDragEnabled(false);

        jTree.getModel().addTreeModelListener(new CategoriesTreeModelListener(controller));

        jTree.setTransferHandler(new DragAndDropHandler(controller, mainFrame));
        jTree.addMouseListener(new JTreeMouseListener(controller));

        jTree.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                int x = (int) e.getPoint().getX();
                int y = (int) e.getPoint().getY();
                TreePath path = jTree.getPathForLocation(x, y);
                if (path == null) {
                    jTree.setCursor(Cursor.getDefaultCursor());
                } else {
                    jTree.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                }
            }
        });
        setRenderer();
    }

    private void initTreeModel() throws BookSystemException {
        m_rootNode = new DefaultMutableTreeNode(controller.getRootDirParent(), true);

        String rootDir = controller.getModel().getProperty().rootDir;
        getList(m_rootNode, new File(rootDir));
        m_model = new DefaultTreeModel(m_rootNode);
    }

    private void setRenderer() {
        ImageIcon folderIcon = new ImageIcon("src/main/resources/icons/treeIcons/folder.PNG");
        DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) jTree.getCellRenderer();
        renderer.setLeafIcon(folderIcon);
        renderer.setClosedIcon(folderIcon);
        renderer.setOpenIcon(folderIcon);
    }

    public JPanel getJPanelWithScroll() {
        jPanel = new JPanel(new BorderLayout());
        jTreeScrollPanel = new JScrollPane(jTree);
        jPanel.add(jTreeScrollPanel, BorderLayout.EAST);
        return jPanel;
    }

    public void updateJPanel() throws BookSystemException {
        jPanel.remove(jTreeScrollPanel);
        init();
        jTreeScrollPanel = new JScrollPane(jTree);
        jPanel.add(jTreeScrollPanel, BorderLayout.EAST);
        jPanel.revalidate();
        jPanel.repaint();
    }

    private void getList(DefaultMutableTreeNode node, File file) {
        if (file.isDirectory() && !file.isHidden()) {
            DefaultMutableTreeNode child = new DefaultMutableTreeNode(file.getName());
            node.add(child);
            File fList[] = file.listFiles();
            if (fList != null) {
                for (File aFList : fList) getList(child, aFList);
            }
        }
    }

    public void remaneSelectedCategoryNode() {
        jTree.setEditable(true);
        DefaultMutableTreeNode selNode = (DefaultMutableTreeNode) jTree.getLastSelectedPathComponent();
        if (selNode == null) {
            return;
        }
        // запоминаем предыдущее имя
        StringBuilder sb = new StringBuilder();
        TreeNode[] p = selNode.getPath();
        for (TreeNode aP : p) sb.append(aP).append("/");
        controller.getModel().setLastCategoryOnRename(sb.toString());

        TreeNode[] nodes = m_model.getPathToRoot(selNode);
        TreePath path = new TreePath(nodes);
        jTree.scrollPathToVisible(path);
        jTree.setSelectionPath(path);
        jTree.startEditingAtPath(path);
        controller.getModel().setCategoryRenamed(true);
    }

    public void deleteSelectedCategoryNode() throws BookSystemException {
        DefaultMutableTreeNode selNode = (DefaultMutableTreeNode) jTree.getLastSelectedPathComponent();
        StringBuilder sb = new StringBuilder();
        TreeNode[] p = selNode.getPath();
        for (TreeNode aP : p) sb.append(aP).append("/");
        File file = new File(sb.toString());
        if (file.exists()) {
            if (file.list().length == 0) {
                boolean success = file.delete();
                if (!success) {
                    throw new BookSystemException("WARNING! \nThe deleting of the directory is failed!");
                }
            } else {
                throw new BookSystemException("ERROR! \nThe directory is not empty!");
            }
        }

        MutableTreeNode parent = (MutableTreeNode) (selNode.getParent());
        if (parent == null) {
            return;
        }
        MutableTreeNode toBeSelNode = selNode.getPreviousSibling();
        if (toBeSelNode == null) {
            toBeSelNode = selNode.getNextSibling();
        }

        if (toBeSelNode == null) {
            toBeSelNode = parent;
        }
        TreeNode[] nodes = m_model.getPathToRoot(toBeSelNode);
        TreePath path = new TreePath(nodes);
        jTree.scrollPathToVisible(path);
        jTree.setSelectionPath(path);
        m_model.removeNodeFromParent(selNode);
    }

    public void addNewCategoryNode() {
        jTree.setEditable(true);
        DefaultMutableTreeNode selNode = (DefaultMutableTreeNode) jTree.getLastSelectedPathComponent();
        if (selNode == null) {
            return;
        }
        DefaultMutableTreeNode newNode = new DefaultMutableTreeNode("Новая папка");
        m_model.insertNodeInto(newNode, selNode, selNode.getChildCount());

        TreeNode[] nodes = m_model.getPathToRoot(newNode);
        TreePath path = new TreePath(nodes);
        jTree.scrollPathToVisible(path);
        jTree.setSelectionPath(path);
        jTree.startEditingAtPath(path);
        controller.getModel().setCategoryAdded(true);
    }
}
