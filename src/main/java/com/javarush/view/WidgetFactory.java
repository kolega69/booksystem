package main.java.com.javarush.view;

import main.java.com.javarush.controller.MainController;
import main.java.com.javarush.controller.Property;
import main.java.com.javarush.controller.locale.EKeys;
import main.java.com.javarush.controller.locale.LocaleEngine;
import main.java.com.javarush.exceptions.BookSystemException;
import main.java.com.javarush.view.component.SearchTextField;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.List;

//not public, only for view package access
class WidgetFactory {

    private static final String RENAME_ACT = "Rename";
	private static final String FAVORITES_ACT = "Favorites";
	private static final String REMOVE_FAVORITES_ACT = "Remove favorites";

    private static JPopupMenu tablePopupMenu;

    private static final String MOVE_BUTTON_IMG_SRC = "src/main/resources/icons/applicationIcons/move.png";
    private static final String SCAN_BUTTON_IMG_SRC = "src/main/resources/icons/applicationIcons/scan.png";
    private static final String SETTINGS_BUTTON_IMG_SRC = "src/main/resources/icons/applicationIcons/settings.png";
    private static final String SEARCH_TEXT_FIELD_IMG_SRC = "src/main/resources/icons/applicationIcons/searchTextField.png";
    private static final String STAR_IMG_SRC = "src/main/resources/icons/applicationIcons/star.png";

    private static JMenuItem renameFileTableMenuItem;
    private static JMenuItem addToFavoritesMenuItem;
    private static JMenuItem removeFromFavoritesMenuItem;
    private static JButton moveButton;
    private static JButton scanButton;
    private static JButton settingsButton;
    private static SearchTextField searchTextField;
    private static JButton nextButton;
    private static JButton prevButton;
    private static JButton nextButtonFavorite;
    private static JButton prevButtonFavorite;
    private static JMenuItem renameNodeCategoriesMenuItem;
    private static JPopupMenu categoriesPopupMenu;
    private static JMenuItem addNodeCategoriesMenuItem;
    private static JMenuItem deleteNodeCategoriesMenuItem;
    private static JMenuBar topMenuBar;
    private static JMenuItem searchMenuItem;
    private static JMenuItem searchWithPathMenuItem;
    private static JMenuItem exitMenuItem;
    private static JMenuItem settingsMenuItem;
    private static JMenu languagesMenu;
    private static JTabbedPane jTabbedPane;
	private static ButtonGroup languageButtons;
    private static JMenuItem helpMenuItem;
    private static JMenuItem aboutMenuItem;

    public static JPopupMenu getTablePopupMenu() {
        if (tablePopupMenu == null) {
            tablePopupMenu = new JPopupMenu();
            tablePopupMenu.add(getRenameFileTableMenuItem());
	        tablePopupMenu.add(getAddToFavoritesMenuItem());
	        tablePopupMenu.add(getRemoveFromFavoritesMenuItem());
        }
        return tablePopupMenu;
    }

	public static void destroyTablePopupMenu() {
		tablePopupMenu = null;
	}

    public static JPopupMenu getCategoriesPopupMenu() {
        if (categoriesPopupMenu == null) {
            categoriesPopupMenu = new JPopupMenu();
            categoriesPopupMenu.add(getRenameNodeCategoriesMenuItem());
            categoriesPopupMenu.add(getAddNodeCategoriesMenuItem());
            categoriesPopupMenu.add(getDeleteNodeCategoriesMenuItem());
        }
        return categoriesPopupMenu;
    }

    public static JMenuItem getRenameFileTableMenuItem() {
        if (renameFileTableMenuItem == null) {
            renameFileTableMenuItem = new JMenuItem();
	        renameFileTableMenuItem.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.RENAME_FILE.getKey());
            renameFileTableMenuItem.setActionCommand(RENAME_ACT);
        }
        return renameFileTableMenuItem;
    }

	public static JMenuItem getAddToFavoritesMenuItem() {
		if (addToFavoritesMenuItem == null) {
			addToFavoritesMenuItem = new JMenuItem();
			addToFavoritesMenuItem.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.ADD_TO_FAVORITES.getKey());
			addToFavoritesMenuItem.setActionCommand(FAVORITES_ACT);
		}

		return addToFavoritesMenuItem;
	}

	public static JMenuItem getRemoveFromFavoritesMenuItem() {
		if (removeFromFavoritesMenuItem == null) {
			removeFromFavoritesMenuItem = new JMenuItem();
			removeFromFavoritesMenuItem.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.REMOVE_FROM_FAVORITES.getKey());
			removeFromFavoritesMenuItem.setActionCommand(REMOVE_FAVORITES_ACT);
		}

		return removeFromFavoritesMenuItem;
	}

    public static JMenuItem getRenameNodeCategoriesMenuItem() {
        if (renameNodeCategoriesMenuItem == null) {
            renameNodeCategoriesMenuItem = new JMenuItem();
	        renameNodeCategoriesMenuItem.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.RENAME_NODE.getKey());
        }
        return renameNodeCategoriesMenuItem;
    }

    public static JMenuItem getAddNodeCategoriesMenuItem() {
        if (addNodeCategoriesMenuItem == null) {
            addNodeCategoriesMenuItem = new JMenuItem();
	        addNodeCategoriesMenuItem.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.ADD_NODE.getKey());
        }
        return addNodeCategoriesMenuItem;
    }

    public static JMenuItem getDeleteNodeCategoriesMenuItem() {
        if (deleteNodeCategoriesMenuItem == null) {
            deleteNodeCategoriesMenuItem = new JMenuItem();
	        deleteNodeCategoriesMenuItem.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.DELETE_NODE.getKey());
        }
        return deleteNodeCategoriesMenuItem;
    }

    public static JButton getMoveButton() {
        if (moveButton == null) {
            ImageIcon moveIcon = new ImageIcon(new ImageIcon(MOVE_BUTTON_IMG_SRC)
                    .getImage().getScaledInstance(20, 20, 100));
            moveButton = new JButton(moveIcon);
            moveButton.setToolTipText(EKeys.MOVE_FILES.getKey());
            moveButton.setActionCommand("Move");
        }
        return moveButton;
    }

    public static JButton getScanButton() {
        if (scanButton == null) {
            ImageIcon searchIcon = new ImageIcon(new ImageIcon(SCAN_BUTTON_IMG_SRC)
                    .getImage().getScaledInstance(20, 20, 100));
            scanButton = new JButton(searchIcon);
            scanButton.setActionCommand("Scan");
        }
        return scanButton;
    }

    public static JButton getSettingsButton() {
        if (settingsButton == null) {
            ImageIcon settingsIcon = new ImageIcon(new ImageIcon(SETTINGS_BUTTON_IMG_SRC)
                    .getImage().getScaledInstance(20, 20, 100));
            settingsButton = new JButton(settingsIcon);
            settingsButton.setActionCommand("Settings");
        }
        return settingsButton;
    }

    public static SearchTextField getSearchTextField(MainController controller) {
        if (searchTextField == null) {
            ImageIcon searchFieldIcon = new ImageIcon(new ImageIcon(SEARCH_TEXT_FIELD_IMG_SRC)
                    .getImage().getScaledInstance(20, 20, 100));
            searchTextField = new SearchTextField(25, controller);
            searchTextField.setIcon(searchFieldIcon);
            searchTextField.setOpaque(false);
        }
        return searchTextField;
    }

    public static JButton getNextButton() {
        if (nextButton == null) {
            nextButton = new JButton();
	        nextButton.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.NEXT_BTN_TITLE.getKey());
            nextButton.setActionCommand("Next");
            nextButton.setEnabled(false);
        }
        return nextButton;
    }

    public static JButton getPrevButton() {
        if (prevButton == null) {
            prevButton = new JButton();
	        prevButton.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.PREV_BTN_TITLE.getKey());
            prevButton.setActionCommand("Prev");
            prevButton.setEnabled(false);
        }
        return prevButton;
    }

    public static JButton getNextButtonFavorite() {
        if (nextButtonFavorite == null) {
            nextButtonFavorite = new JButton();
	        nextButtonFavorite.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.NEXT_BTN_TITLE.getKey());
            nextButtonFavorite.setActionCommand("Next");
            nextButtonFavorite.setEnabled(false);
        }
        return nextButtonFavorite;
    }

    public static JButton getPrevButtonFavorite() {
        if (prevButtonFavorite == null) {
            prevButtonFavorite = new JButton();
	        prevButtonFavorite.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.PREV_BTN_TITLE.getKey());
            prevButtonFavorite.setActionCommand("Prev");
            prevButtonFavorite.setEnabled(false);
        }
        return prevButtonFavorite;
    }

    public static JMenuBar getTopMenuBar() {
        if (topMenuBar == null) {
            topMenuBar = new JMenuBar();

            // creates main menu
            JMenu mainMenu = new JMenu("");
	        mainMenu.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.MENU_TITLE.getKey());
            mainMenu.setMnemonic(KeyEvent.VK_M);

            mainMenu.add(getScanMenuItem());
            mainMenu.add(getScanWithPathMenuItem());
            mainMenu.addSeparator();
            mainMenu.add(getExitMenuItem());

            //creates config menu;
            JMenu configMenu = new JMenu("");
	        configMenu.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.CONFIG_MENU_TITLE.getKey());
            configMenu.setMnemonic(KeyEvent.VK_C);
            configMenu.add(getSettingsMenuItem());
	        configMenu.add(getLanguagesMenu());

            //creates help menu;
            JMenu helpMenu = new JMenu("");
            helpMenu.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.HELP_MENU_TITLE.getKey());
            helpMenu.setMnemonic(KeyEvent.VK_H);
            helpMenu.add(getHelpMenuItem());
            helpMenu.add(getAboutMenuItem());

	        getLanguagesBtnGroup();

            topMenuBar.add(mainMenu);
            topMenuBar.add(configMenu);
            topMenuBar.add(helpMenu);
        }
        return topMenuBar;
    }

    public static JMenuItem getSettingsMenuItem() {
        if (settingsMenuItem == null) {
            settingsMenuItem = new JMenuItem("", KeyEvent.VK_O);
	        settingsMenuItem.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.OPTIONS_MENU_TITLE.getKey());
            settingsMenuItem.setAccelerator(KeyStroke.getKeyStroke(
                    KeyEvent.VK_O, InputEvent.CTRL_MASK));
        }
        return settingsMenuItem;
    }

	public static JMenu getLanguagesMenu() {
		if (languagesMenu == null) {
			languagesMenu = new JMenu("");
			languagesMenu.setMnemonic(KeyEvent.VK_L);
			languagesMenu.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.LANGUAGES_MENU_TITLE.getKey());
		}
		return languagesMenu;
	}

    public static JMenuItem getExitMenuItem() {
        if (exitMenuItem == null) {
            exitMenuItem = new JMenuItem("", KeyEvent.VK_X);
	        exitMenuItem.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.EXIT_MENU_TITLE.getKey());
            exitMenuItem.setAccelerator(KeyStroke.getKeyStroke(
                    KeyEvent.VK_X, InputEvent.CTRL_MASK));
        }
        return exitMenuItem;
    }

    public static JMenuItem getScanWithPathMenuItem() {
        if (searchWithPathMenuItem == null) {
            searchWithPathMenuItem = new JMenuItem("");
	        searchWithPathMenuItem.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.SCAN_WITH_PATH_MENU_TITLE.getKey());
        }
        return searchWithPathMenuItem;
    }

    public static JMenuItem getScanMenuItem() {
        if (searchMenuItem == null) {
            searchMenuItem = new JMenuItem("", KeyEvent.VK_S);
	        searchMenuItem.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.SCAN_MENU_TITLE.getKey());
            searchMenuItem.setAccelerator(KeyStroke.getKeyStroke(
                    KeyEvent.VK_S, InputEvent.CTRL_MASK));
        }
        return searchMenuItem;
    }

	public static ButtonGroup getLanguagesBtnGroup() {
		if (languageButtons == null) {
			languageButtons = new ButtonGroup();
			try {
				List<String> languages = Property.getInstance().getLanguagesBunch();
				String currentLanguage = Property.getInstance().getCurrentLanguage();
				for (String lang : languages) {
					JRadioButtonMenuItem langMItem = new JRadioButtonMenuItem("", false);
					langMItem.putClientProperty(LocaleEngine.LOCALIZATION_KEY, "lang.button." + lang);
					langMItem.setActionCommand(lang);
					languageButtons.add(langMItem);
					getLanguagesMenu().add(langMItem);
					if (lang.equals(currentLanguage)) {
						langMItem.setSelected(true);
					}
				}
			} catch (BookSystemException e) {
				e.printStackTrace();
			}


		}
		return languageButtons;
	}

    public static JTabbedPane getTabbedPane() {
        if(jTabbedPane == null) {
            jTabbedPane = new JTabbedPane(JTabbedPane.TOP);
        }
        return jTabbedPane;
    }

	public static ImageIcon getStarIcon() {
		return new ImageIcon(new ImageIcon(STAR_IMG_SRC).getImage().getScaledInstance(15, 15, 100));
	}

    public static JMenuItem getHelpMenuItem() {
        if (helpMenuItem == null) {
            helpMenuItem = new JMenuItem("", KeyEvent.VK_H);
            helpMenuItem.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.HELP_MENU_TITLE.getKey());
            helpMenuItem.setAccelerator(KeyStroke.getKeyStroke(
                    KeyEvent.VK_F1, InputEvent.BUTTON1_MASK));
        }
        return helpMenuItem;
    }

    public static JMenuItem getAboutMenuItem() {
        if (aboutMenuItem == null){
            aboutMenuItem = new JMenuItem("", KeyEvent.VK_A);
            aboutMenuItem.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.ABOUT_MENU_TITLE.getKey());
        }
        return aboutMenuItem;
    }

}
