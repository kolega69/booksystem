package main.java.com.javarush.view;

import main.java.com.javarush.controller.MainController;
import main.java.com.javarush.controller.Property;
import main.java.com.javarush.exceptions.BookSystemException;

import javax.swing.*;
import java.io.File;
import java.util.Locale;

/**
 * Created by KOlegA on 10.01.14.
 * This class provides window for choose directory with book files
 */
public class PathChooser {

    public static String getUserPath(MainController controller) {
        JFileChooser jfch = new JFileChooser();

        jfch.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int result = jfch.showDialog(null, controller.getChooseBtnText());

        if (result != JFileChooser.APPROVE_OPTION) return null;

        File tempFile = jfch.getSelectedFile();

        if (!tempFile.exists()) {
            JOptionPane.showMessageDialog(null, controller.getSpecifyDirectoryText(),
		            controller.getDirNotExistText(), JOptionPane.WARNING_MESSAGE);
            return null;
        }

        return tempFile.getPath();
    }
}
