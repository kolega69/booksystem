package main.java.com.javarush.view;

import main.java.com.javarush.controller.MainController;
import main.java.com.javarush.controller.locale.EKeys;
import main.java.com.javarush.controller.locale.LocaleEngine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by Serge on 08.03.14.
 */
public class BookSystemAbout extends JDialog {
    private JPanel panel;
    private JLabel label;

    public BookSystemAbout(JFrame owner, String title, boolean modal, final MainController controller) {
        super(owner, title, modal);
        setSize(250, 200);
        setLocation(owner.getX() + 250, owner.getY() + 250);
        setResizable(false);

        GridBagLayout gbl = new GridBagLayout();
        panel = new JPanel();
        panel.setLayout(gbl);

        GridBagConstraints c = new GridBagConstraints();

        c.insets = new Insets(0, 0, 20, 0);
        JLabel titleLabel = new JLabel("BookSystem V1.0  ");
        titleLabel.setFont(new Font("SanSerif",Font.PLAIN,20));
        addWidgetToContainerAndAddToPathPanel(gbl, c, titleLabel, 0, 0);

        c.insets = new Insets(0, 0, 0, 0);
        label = new JLabel(controller.getTextAbout());
        addWidgetToContainerAndAddToPathPanel(gbl, c, label, 0, 1);

        c.insets = new Insets(0, 40, 20, 0);
        JLabel label1 = new JLabel("<html><a href=\"http://javarush.ru/main.html\"> JavaRush.ru</a></html>");
        label1.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 1 && e.getButton() == MouseEvent.BUTTON1) {
                    String url = "http://javarush.ru/main.html";
                    controller.openFileWithNativeOS(url);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        addWidgetToContainerAndAddToPathPanel(gbl, c, label1, 0, 2);
        add(panel);
    }

    public void applyLocale (LocaleEngine localeEngine) {
        setTitle(localeEngine.getStringResource(EKeys.ABOUT_MENU_TITLE.getKey()));
        label.setText(localeEngine.getStringResource(EKeys.TEXT_ABOUT.getKey()));
    }

    private void addWidgetToContainerAndAddToPathPanel(GridBagLayout gbl, GridBagConstraints c,
                                                       Component component, int x, int y) {
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = x;
        c.gridy = y;
        gbl.setConstraints(component, c);
        panel.add(component);
    }
}
