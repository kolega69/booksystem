package main.java.com.javarush.view.book;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: Serge
 * Date: 10.01.14
 * Time: 20:25
 */
public enum BookIcon {
    txt("src/main/resources/icons/extensionIcons/txt.png"),
    doc("src/main/resources/icons/extensionIcons/doc.png"),
    docx("src/main/resources/icons/extensionIcons/docx.png"),
    djvu("src/main/resources/icons/extensionIcons/djvu.png"),
    pdf("src/main/resources/icons/extensionIcons/pdf.png"),
    rtf("src/main/resources/icons/extensionIcons/rtf.png"),
    epub("src/main/resources/icons/extensionIcons/epub.png"),
	fb2("src/main/resources/icons/extensionIcons/fb2.png");

    private String path;

    private BookIcon(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public static Object getIcon(String name) {
        String extension = name.substring(name.lastIndexOf(".") + 1);

        try {
            BookIcon bookIcon = BookIcon.valueOf(extension.toLowerCase());
            return new ImageIcon(new ImageIcon(bookIcon.getPath()).getImage().getScaledInstance(40, 40, 200));
        } catch (IllegalArgumentException e) {
            return extension;
        }


    }
}



