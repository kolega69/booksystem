package main.java.com.javarush.view.book;

import main.java.com.javarush.controller.MainController;
import main.java.com.javarush.controller.Property;
import main.java.com.javarush.controller.locale.EKeys;
import main.java.com.javarush.controller.locale.LocaleEngine;
import main.java.com.javarush.exceptions.BookSystemException;
import main.java.com.javarush.view.PathChooser;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Collections;
import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: Serge
 * Date: 14.01.14
 * Time: 19:50
 */
public class BookSettings extends JDialog {
    private Property property;
    private TextField homeDir;
    private TextField tExceptPath;
    private TextField pathsForSearch;
    private String extens;
    private JSlider quantity;
    private JPanel pathsPanel;
    private JPanel othersPanel;
    private java.util.List<String> extensions;

    private static JButton okButton;
    private static JButton cancelButton;
    private static JTabbedPane tabbedPane;

    private MainController controller;

    public BookSettings(Frame owner, String title, boolean modal, MainController controller) throws
            BookSystemException
    {
        super(owner, title, modal);
        this.controller = controller;

        setSize(450, 400);
        setLocation(owner.getX() + 100, owner.getY() + 100);
        setResizable(false);

        property = Property.getInstance();
        extens = property.fileExtensions;

        Border border = BorderFactory.createEmptyBorder(10, 10, 10, 10);

        tabbedPane = tabs();
        tabbedPane.setBorder(border);
        JPanel but = acceptButtons();
        but.setBorder(border);
        add(tabbedPane, BorderLayout.CENTER);
        add(but, BorderLayout.SOUTH);
    }

    private JTabbedPane tabs() {

        JTabbedPane tabs = new JTabbedPane();

        tabs.addTab("Paths", getPathsPanel());
        tabs.addTab("Others", getOthersPanel());

        return tabs;
    }

    /**
     * Creates panel with paths.
     *
     * @return JPanel
     */

    private JPanel getPathsPanel() {
        pathsPanel = new JPanel();

        GridBagLayout gbl = new GridBagLayout();
        pathsPanel.setLayout(gbl);

        GridBagConstraints c = new GridBagConstraints();

        c.insets = new Insets(0, 5, 0, 0);

        JLabel labelHomeDir = new JLabel();
        labelHomeDir.setText("");
	    labelHomeDir.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.LABEL_HOME.getKey());
        addWidgetToContainerAndAddToPathPanel(gbl, c, labelHomeDir, 0, 0);

        homeDir = new TextField(30);
        homeDir.setText(property.rootDir);
        c.gridwidth = 2;
        addWidgetToContainerAndAddToPathPanel(gbl, c, homeDir, 0, 1);

        JButton buttonPath1 = new JButton("Path");
        buttonPath1.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.PATH_BTN.getKey());
        buttonPath1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String pathChooser = PathChooser.getUserPath(controller);
                if (!(pathChooser == null)) homeDir.setText(pathChooser);
            }
        });
        addWidgetToContainerAndAddToPathPanel(gbl, c, buttonPath1, 2, 1);

        c.insets = new Insets(10, 5, 0, 0);

        JLabel labelPathForSearch = new JLabel();
        labelPathForSearch.setText("");
	    labelPathForSearch.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.LABEL_SEARCH.getKey());
        addWidgetToContainerAndAddToPathPanel(gbl, c, labelPathForSearch, 0, 2);

        c.insets = new Insets(0, 5, 0, 0);

        pathsForSearch = new TextField(30);
        pathsForSearch.setText(property.paths);
        c.gridwidth = 2;
        addWidgetToContainerAndAddToPathPanel(gbl, c, pathsForSearch, 0, 3);

        JButton buttonPath = new JButton("Path");
        buttonPath.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.PATH_BTN.getKey());
        buttonPath.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String pathChooser = PathChooser.getUserPath(controller);
                if (!(pathChooser == null)) pathsForSearch.setText(pathChooser);
            }
        });
        addWidgetToContainerAndAddToPathPanel(gbl, c, buttonPath, 2, 3);

        JButton buttonAdd = new JButton("Add path");
	    buttonAdd.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.ADD_PATH_BTN.getKey());
        buttonAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pathsForSearch.setText(addPath(pathsForSearch.getText()));
            }
        });
        addWidgetToContainerAndAddToPathPanel(gbl, c, buttonAdd, 4, 3);


        c.insets = new Insets(10, 5, 0, 0);

        JLabel labelExceptPath = new JLabel();
        labelExceptPath.setText("Except path:");
	    labelExceptPath.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.LABEL_EXEPT.getKey());
        addWidgetToContainerAndAddToPathPanel(gbl, c, labelExceptPath, 0, 6);

        c.insets = new Insets(0, 5, 0, 0);

        tExceptPath = new TextField(30);
        tExceptPath.setText(property.exceptPath);
        c.gridwidth = 2;
        addWidgetToContainerAndAddToPathPanel(gbl, c, tExceptPath, 0, 7);

        JButton buttonPath2 = new JButton("Path");
        buttonPath2.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.PATH_BTN.getKey());
        buttonPath2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String pathChooser = PathChooser.getUserPath(controller);
                if (!(pathChooser == null)) tExceptPath.setText(pathChooser);
            }
        });
        addWidgetToContainerAndAddToPathPanel(gbl, c, buttonPath2, 2, 7);

        JButton buttonAdd2 = new JButton("Add path");
	    buttonAdd2.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.ADD_PATH_BTN.getKey());
        buttonAdd2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tExceptPath.setText(addPath(tExceptPath.getText()));
            }
        });
        addWidgetToContainerAndAddToPathPanel(gbl, c, buttonAdd2, 4, 7);

        return pathsPanel;
    }

    private void addWidgetToContainerAndAddToPathPanel(GridBagLayout gbl, GridBagConstraints c, Component component, int x, int y) {
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = x;
        c.gridy = y;
        gbl.setConstraints(component, c);
        pathsPanel.add(component);
    }

    private JPanel getOthersPanel() {
        othersPanel = new JPanel();
        othersPanel.setSize(450, 200);

        Border border = BorderFactory.createEmptyBorder(10, 10, 10, 10);

        Box box1 = Box.createVerticalBox();   //box with checkboxes
        Box box2 = Box.createVerticalBox();   //box with slider

        box1.setBorder(border);
        box2.setBorder(border);

        JPanel ext = checkboxPanel();

        JLabel labelExtension = new JLabel("");
	    labelExtension.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.LABEL_FILE_EXTENSIONS.getKey());
        labelExtension.setAlignmentX(JLabel.CENTER_ALIGNMENT);

        JLabel labelNumberOfRows = new JLabel("");
	    labelNumberOfRows.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.LABEL_NUMBER_OF_ROWS.getKey());
        labelNumberOfRows.setAlignmentX(JLabel.CENTER_ALIGNMENT);

        quantity = setQuntitySlider();      //sets slider for books per page.

        box1.add(labelExtension);
        box1.add(Box.createVerticalStrut(3));
        box1.add(ext);

        box2.add(labelNumberOfRows);
        box2.add(Box.createVerticalStrut(10));
        box2.add(quantity);

        othersPanel.add(box1);
        othersPanel.add(box2);

        return othersPanel;
    }

    /**
     * Creates panel with Ok and Cancel buttons
     *
     * @return JPanel with buttons
     */
    private JPanel acceptButtons() {

        JPanel jPanel = new JPanel();

        FlowLayout fl = new FlowLayout();
        jPanel.setLayout(fl);
        fl.setAlignment(FlowLayout.RIGHT);

        //OK button
        JButton buttonOK = getOkButton();
        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    boolean flag = false;
                    Property properties = Property.getInstance();
                    if (!properties.rootDir.equals(homeDir.getText())) {
                        properties.rootDir = homeDir.getText();
                        flag = true;
                        controller.refreshJTreeView();
                    }

                    if (!properties.paths.equals(pathsForSearch.getText())) {
                        properties.paths = pathsForSearch.getText();
                        flag = true;
                    }

                    if (!properties.booksPerPage.equals(String.valueOf(quantity.getValue()))) {
                        properties.booksPerPage = String.valueOf(quantity.getValue());
                        flag = true;
                    }

                    if (!properties.exceptPath.equals(tExceptPath.getText())) {
                        properties.exceptPath = tExceptPath.getText();
                        flag = true;
                    }

                    if (!properties.fileExtensions.equals(extens)) {
                        String[] exten = properties.fileExtensions.split(",");
                        String[] str = extens.split(",");
                        java.util.List<String> fExten = new LinkedList<String>();
                        java.util.List<String> fStr = new LinkedList<String>();
                        Collections.addAll(fExten, exten);
                        Collections.addAll(fStr, str);
                        if (fExten.size() != fStr.size()) {
                            properties.fileExtensions = extens;
                            flag = true;
                        } else
                            if (!fExten.containsAll(fStr)) {
                                properties.fileExtensions = extens;
                                flag = true;
                            }
                    }
                    if (flag) {
                        properties.saveProperty();
                    }
                } catch (BookSystemException e1) {
                    controller.showExceptionToUser(e1);
                }

                BookSettings.this.setVisible(false);
                BookSettings.this.dispose();

            }
        });

        //Cancel button
        JButton buttonCancel = getCancelButton();
        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BookSettings.this.setVisible(false);
                BookSettings.this.dispose();
            }
        });

        jPanel.add(buttonOK);
        jPanel.add(buttonCancel);

        return jPanel;
    }

    /**
     * Creates checkboxes. User can choose which extensions
     * application must to search.
     *
     * @return instance of JPanel with checkboxes.
     */
    private JPanel checkboxPanel() {
        JPanel panel = new JPanel();

        extensions = property.bookExtensions;

        JCheckBox txtBox = new JCheckBox("txt", extensions.contains("txt"));
        JCheckBox pdfBox = new JCheckBox("pdf", extensions.contains("pdf"));
        JCheckBox djvuBox = new JCheckBox("djvu", extensions.contains("djvu"));
        JCheckBox docBox = new JCheckBox("doc", extensions.contains("doc"));
        JCheckBox docxBox = new JCheckBox("docx", extensions.contains("docx"));
        JCheckBox fb2Box = new JCheckBox("fb2", extensions.contains("fb2"));
        JCheckBox epubBox = new JCheckBox("epub", extensions.contains("epub"));
	    JCheckBox rtfBox = new JCheckBox("rtf", extensions.contains("rtf"));

        ItemListener il = new CheckboxListener();

        txtBox.addItemListener(il);
        pdfBox.addItemListener(il);
        djvuBox.addItemListener(il);
        docBox.addItemListener(il);
        docxBox.addItemListener(il);
        fb2Box.addItemListener(il);
        epubBox.addItemListener(il);
	    rtfBox.addItemListener(il);

        panel.add(txtBox);
        panel.add(pdfBox);
        panel.add(djvuBox);
        panel.add(docBox);
        panel.add(docxBox);
        panel.add(fb2Box);
        panel.add(epubBox);
	    panel.add(rtfBox);

        return panel;
    }

    /**
     * Creates slider for setting quantity of books per page.
     *
     * @return instance of slider.
     */
    private JSlider setQuntitySlider() {

        JSlider jsldr = new JSlider(5, 30, Integer.parseInt(property.booksPerPage));

        jsldr.setMajorTickSpacing(5);
        jsldr.setLabelTable(jsldr.createStandardLabels(5));

        jsldr.setPaintLabels(true);
        jsldr.setPaintTicks(true);

        jsldr.setSnapToTicks(true);

        return jsldr;
    }

    /**
     * This listener checks state of extension checkboxes
     * and put changes in extens string.
     */

    private class CheckboxListener implements ItemListener {
        @Override
        public void itemStateChanged(ItemEvent e) {
            JCheckBox cb = (JCheckBox) e.getItem();
            String ext = cb.getActionCommand();

            if (cb.isSelected()) {
                extensions.add(ext);
            } else {
                extensions.remove(ext);
            }

            StringBuilder extBuf = new StringBuilder();
            for (int i = 0; i < extensions.size(); i++) {
                extBuf.append(extensions.get(i));
                if (i == extensions.size() - 1) break;
                extBuf.append(",");
            }

            extens = extBuf.toString();
        }
    }

    private String addPath(String path) {
        String pathChooser = PathChooser.getUserPath(controller);
        StringBuilder builder = new StringBuilder();
        builder.append(path);
        if (!(pathChooser == null) && !builder.toString().contains(pathChooser)) {
            builder.append(";").append(pathChooser);
        }
        return builder.toString();
    }

    public static JButton getOkButton() {
        if (okButton == null) {
            okButton = new JButton();
            okButton.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.OK_BTN.getKey());
        }
        return okButton;
    }

    public static JButton getCancelButton() {
        if (cancelButton == null) {
            cancelButton = new JButton();
            cancelButton.putClientProperty(LocaleEngine.LOCALIZATION_KEY, EKeys.CANCEL_BTN.getKey());
        }
        return cancelButton;
    }

    public void applyLocale(LocaleEngine localeEngine) {
        localeEngine.changeLocale(getContentPane());
        setTitle(localeEngine.getStringResource(EKeys.SETTINGS_TITLE.getKey()));
        tabbedPane.setTitleAt(0, localeEngine.getStringResource(EKeys.PATHS_TAB.getKey()));
        tabbedPane.setTitleAt(1, localeEngine.getStringResource(EKeys.OTHERS_TAB.getKey()));
    }
}
