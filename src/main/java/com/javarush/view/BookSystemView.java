package main.java.com.javarush.view;

import main.java.com.javarush.controller.MainController;
import main.java.com.javarush.controller.Property;
import main.java.com.javarush.controller.locale.EKeys;
import main.java.com.javarush.controller.locale.LocaleEngine;
import main.java.com.javarush.exceptions.BookSystemException;
import main.java.com.javarush.view.component.BookSystemCategoryTree;
import main.java.com.javarush.view.component.SearchTextField;
import main.java.com.javarush.view.component.table.BookSystemTable;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Locale;

public class BookSystemView {
    private static final String BOOK_SYSTEM_APPLICATION_TITLE = "BookSystem v1.0";
    private MainController controller;

    private Border border = BorderFactory.createEtchedBorder();
    private JFrame mainFrame;
    private JLabel page;
    private JLabel pageFavorite;
    private BookSystemTable tableBooks;
    private BookSystemTable favoriteBooks;
    private BookSystemCategoryTree bookSystemTree;
    private JPopupMenu tablePopupMenu;
    private JPopupMenu categoriesPopupMenu;
    private SearchTextField searchTextField;
    private JButton scanButton;
    private JButton settingsButton;
    private JButton moveButton;
    private JButton next;
    private JButton prev;
    private JButton nextFavorite;
    private JButton prevFavorite;
    private JTabbedPane tabbedPane;

	private static String WARNING_TITLE;
	private static String OVERWRITE_MESSAGE;
	private static String NOT_CORRECT_MESSAGE;

    public BookSystemView(MainController controller) {
        this.controller = controller;
    }

    public void init() throws BookSystemException {
        Dimension ScreenSize = Toolkit.getDefaultToolkit().getScreenSize();

        int x = ScreenSize.width / 2 - 600;
        int y = ScreenSize.height / 2 - 300;

        mainFrame = new JFrame(BOOK_SYSTEM_APPLICATION_TITLE);

        mainFrame.setSize(700, 600);
        mainFrame.setLocation(x, y);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        tableBooks = new BookSystemTable(controller);
        tableBooks.init();
        tablePopupMenu = WidgetFactory.getTablePopupMenu();
	    WidgetFactory.getRenameFileTableMenuItem().addActionListener(new AbstractAction() {
		    @Override
		    public void actionPerformed(ActionEvent e) {
			    controller.renameSelectedFile();
                controller.getModel().setTableEditable(false);
		    }
	    });
	    WidgetFactory.getAddToFavoritesMenuItem().addActionListener(new AbstractAction() {
		    @Override
		    public void actionPerformed(ActionEvent e) {
			    controller.addFileToFavorites();
		    }
	    });
	    WidgetFactory.getRemoveFromFavoritesMenuItem().addActionListener(new AbstractAction() {
		    @Override
		    public void actionPerformed(ActionEvent e) {
			    controller.removeFileFromFavorites();
		    }
	    });


        JScrollPane scrollPane = new JScrollPane(tableBooks);
        tableBooks.setPreferredScrollableViewportSize(new Dimension(600, 300));

        favoriteBooks = new BookSystemTable(controller);
        favoriteBooks.init();
	    favoriteBooks.setDragEnabled(false);

        JScrollPane scrollPaneFavorite = new JScrollPane(favoriteBooks);
        favoriteBooks.setPreferredScrollableViewportSize(new Dimension(600, 300));

        JPanel mainTablePanel = new JPanel(new BorderLayout());
        JPanel favoriteTablePanel = new JPanel(new BorderLayout());
        mainTablePanel.add(scrollPane);
        favoriteTablePanel.add(scrollPaneFavorite);

        scanButton = WidgetFactory.getScanButton();
        settingsButton = WidgetFactory.getSettingsButton();

        // кнопка перемещения книг
        moveButton = WidgetFactory.getMoveButton();

        // поле для поиска книг
        searchTextField = WidgetFactory.getSearchTextField(controller);

        JMenuBar jmb = WidgetFactory.getTopMenuBar();
        WidgetFactory.getScanMenuItem().addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.scanBooks(null);
            }
        });
        WidgetFactory.getScanWithPathMenuItem().addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String userPath = PathChooser.getUserPath(controller);
                if (userPath != null) {
                    controller.scanBooks(userPath);
                }

            }
        });
        WidgetFactory.getExitMenuItem().addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.exitApp();
            }
        });
        WidgetFactory.getSettingsMenuItem().addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.showSettingsMenu();
                controller.checkIsFoldersExist();
            }
        });
        WidgetFactory.getHelpMenuItem().addActionListener(new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                controller.showHelp();
            }
        });
        WidgetFactory.getAboutMenuItem().addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.showAbout();
            }
        });

        Component[] langMItem = WidgetFactory.getLanguagesMenu().getMenuComponents();
	    for (Component langBtn : langMItem) {

		    if (!(langBtn instanceof JRadioButtonMenuItem)) continue;
		    JRadioButtonMenuItem btn = (JRadioButtonMenuItem) langBtn;
		    final String lang = btn.getActionCommand();

		    btn.addActionListener(new ActionListener() {
			    @Override
			    public void actionPerformed(ActionEvent e) {
				    try {
					    Property prop = Property.getInstance();
					    prop.setCurrentLanguage(lang);
					    prop.saveProperty();
					    controller.applyLocale(lang);
				    } catch (BookSystemException e1) {
					    e1.printStackTrace();
				    }
			    }
		    });
	    }

        jmb.setBorder(border);

        mainFrame.add(jmb, BorderLayout.NORTH);

        JToolBar jtb = new JToolBar();
        jtb.add(scanButton);
        jtb.add(settingsButton);
        jtb.add(searchTextField);

        mainFrame.add(jtb, BorderLayout.NORTH);
        mainFrame.setJMenuBar(jmb);

        page = new JLabel();
        page.setHorizontalAlignment(JLabel.CENTER);
        page.setVerticalAlignment(JLabel.CENTER);

        pageFavorite = new JLabel();
        pageFavorite.setHorizontalAlignment(JLabel.CENTER);
        pageFavorite.setVerticalAlignment(JLabel.CENTER);

        // кнопка вперед
        next = WidgetFactory.getNextButton();
        // кнопка назад
        prev = WidgetFactory.getPrevButton();

        JPanel buttonsMain = new JPanel(new BorderLayout());
        buttonsMain.add(next, BorderLayout.EAST);
        buttonsMain.add(prev, BorderLayout.WEST);
        buttonsMain.add(page);
        mainTablePanel.add(buttonsMain, BorderLayout.SOUTH);

        JPanel buttonsFavorite = new JPanel(new BorderLayout());
        nextFavorite = WidgetFactory.getNextButtonFavorite();
        prevFavorite = WidgetFactory.getPrevButtonFavorite();

        buttonsFavorite.add(nextFavorite, BorderLayout.EAST);
        buttonsFavorite.add(prevFavorite, BorderLayout.WEST);
        buttonsFavorite.add(pageFavorite);

        favoriteTablePanel.add(buttonsFavorite, BorderLayout.SOUTH);
        tabbedPane = WidgetFactory.getTabbedPane();
        tabbedPane.addTab("Books", mainTablePanel);
        tabbedPane.addTab("Favorite",WidgetFactory.getStarIcon(), favoriteTablePanel);
        mainFrame.add(tabbedPane);

        // Дерево файлов
        bookSystemTree = new BookSystemCategoryTree(controller, mainFrame);
        bookSystemTree.init();

        mainFrame.add(bookSystemTree.getJPanelWithScroll(), BorderLayout.EAST);

        categoriesPopupMenu = WidgetFactory.getCategoriesPopupMenu();

        WidgetFactory.getAddNodeCategoriesMenuItem().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                controller.onAddNodeCategoriesMenuAction();
            }
        });

        WidgetFactory.getDeleteNodeCategoriesMenuItem().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                controller.onDeleteNodeCategoriesMenuAction();
            }
        });

        WidgetFactory.getRenameNodeCategoriesMenuItem().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                controller.onRenameNodeCategoriesMenuAction();
            }
        });


        tabbedPane.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JTabbedPane sourceTabbedPane = (JTabbedPane) e.getSource();
                int index = sourceTabbedPane.getSelectedIndex();
                if(index == 1) {
                    controller.setIsFavoriteTab(true);
                    controller.showFavoriteBooks();
                }
                if(index == 0) {
                    controller.setIsFavoriteTab(false);
                    controller.showBooks();
                }
            }
        });

        scanButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.scanBooks(null);
            }
        });
        settingsButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.showSettingsMenu();
                controller.checkIsFoldersExist();
            }
        });
        moveButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.moveFiles();
            }
        });
        next.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.goToNextPage();
            }
        });
        prev.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.goToPrevPage();
            }
        });
        nextFavorite.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.goToNextPage();
            }
        });
	    prevFavorite.addActionListener(new AbstractAction() {
		    @Override
		    public void actionPerformed(ActionEvent e) {
			    controller.goToPrevPage();
		    }
	    });
	    searchTextField.addCaretListener(new CaretListener() {
		    @Override
		    public void caretUpdate(CaretEvent e) {
			    controller.getView().getTabbedPane().setSelectedIndex(0);
			    String bookName = searchTextField.getText();
			    controller.getModel().setSearchQueryText(bookName);
			    controller.searchBooksInsideTable();
		    }
	    });

	    controller.applyLocale(Property.getInstance().getCurrentLanguage());
    }

    public void refreshJTree() throws BookSystemException {
        bookSystemTree.updateJPanel();
    }

    public void refresh(java.util.List<File> files) {
        if(!controller.getIsFavoriteTab()) {
            if (files.isEmpty()) {
                page.setText("");
            }
            tableBooks.refreshData(files);
        }
        else {
            if(files.isEmpty()) {
                pageFavorite.setText("");
            }
            favoriteBooks.refreshData(files);
        }
    }

    public void renameSelectedFile() {
        if(!controller.getIsFavoriteTab()) {
            tableBooks.renameSelectedFile();
        }
        else {
            favoriteBooks.renameSelectedFile();
        }
    }

    public JFrame getMainFrame() {
        return mainFrame;
    }

    public void renameSelectedCategory() {
        bookSystemTree.remaneSelectedCategoryNode();
    }

    public void deleteSelectedCategoryNode() throws BookSystemException {
        bookSystemTree.deleteSelectedCategoryNode();
    }

    public void addNewCategoryNode() {
        bookSystemTree.addNewCategoryNode();
    }

    public void show() {
        mainFrame.setVisible(true);
    }

    public void showException(BookSystemException e) {
        JOptionPane.showMessageDialog(new JFrame(), e.getMessage());
    }

    public void disableEnableNavigationButtons(boolean nextEnabled, boolean prevEnabled) {
        if(!controller.getIsFavoriteTab()) {
            next.setEnabled(nextEnabled);
            prev.setEnabled(prevEnabled);
        } else {
            nextFavorite.setEnabled(nextEnabled);
            prevFavorite.setEnabled(prevEnabled);
        }
    }

    public void refreshPagerUIData(int currentPage, int pageCount) {
        if(!controller.getIsFavoriteTab()) {
            page.setText(currentPage + "/" + pageCount);
        }
        else {
            pageFavorite.setText(currentPage + "/" + pageCount);
        }
    }

	public BookSystemTable getTableBooks() {
		return tableBooks;
	}

    public BookSystemTable getFavoriteBooks() {
        return favoriteBooks;
    }

    public JTabbedPane getTabbedPane() {
        return tabbedPane;
    }

    public void showTablePopupMenu(Component component, int x, int y) {
	    tablePopupMenu.show(component, x, y);
    }

    public void addToFavoritesMenuItem() {
	    tablePopupRevive();
        tablePopupMenu.add(WidgetFactory.getAddToFavoritesMenuItem());
    }

	public void addRemoveFromIndexMenuItem() {
		tablePopupRevive();
		tablePopupMenu.add(WidgetFactory.getRemoveFromFavoritesMenuItem());
	}

	public void removeMenuItemAddToFavorites() {
		tablePopupRevive();
		tablePopupMenu.remove(WidgetFactory.getAddToFavoritesMenuItem());
	}

	public void removeMenuItemRemoveFromFavorites() {
		tablePopupRevive();
		tablePopupMenu.remove(WidgetFactory.getRemoveFromFavoritesMenuItem());
	}

	private void tablePopupRevive() {
		if (tablePopupMenu == null) tablePopupMenu = WidgetFactory.getTablePopupMenu();
	}

    public void showCategoriesPopupMenu(Component component, int x, int y) {
        categoriesPopupMenu.show(component, x, y);
    }

	public void applyLocale (LocaleEngine localeEngine) {
		localeEngine.changeLocale(getMainFrame().getContentPane());
		localeEngine.changeLocale(getMainFrame().getJMenuBar());

		String tableName = localeEngine.getStringResource(EKeys.TABLE_CELL_NAME.getKey());
		String tableType = localeEngine.getStringResource(EKeys.TABLE_CELL_TYPE.getKey());

		tableBooks.getColumnModel().getColumn(0).setHeaderValue(tableType);
		tableBooks.getColumnModel().getColumn(1).setHeaderValue(tableName);
		tableBooks.repaint();

		favoriteBooks.getColumnModel().getColumn(0).setHeaderValue(tableType);
		favoriteBooks.getColumnModel().getColumn(1).setHeaderValue(tableName);
		favoriteBooks.repaint();

		tabbedPane.setTitleAt(0, localeEngine.getStringResource(EKeys.BOOKS_TAB.getKey()));
		tabbedPane.setTitleAt(1, localeEngine.getStringResource(EKeys.FAVORITES_TAB.getKey()));

		WidgetFactory.destroyTablePopupMenu();
		tablePopupMenu = null;
		localeEngine.changeLocale(WidgetFactory.getTablePopupMenu());

		localeEngine.changeLocale(WidgetFactory.getCategoriesPopupMenu());

		searchTextField.setToolTipText(localeEngine.getStringResource(EKeys.SEARCH_FIELD_TIP.getKey()));
		scanButton.setToolTipText(localeEngine.getStringResource(EKeys.SCAN_FILES.getKey()));
		settingsButton.setToolTipText(localeEngine.getStringResource(EKeys.SETTINGS.getKey()));

		OVERWRITE_MESSAGE = localeEngine.getStringResource((EKeys.OVERWRITE_MASSAGE.getKey()));
		NOT_CORRECT_MESSAGE = localeEngine.getStringResource(EKeys.NOT_CORRECT_MASSAGE.getKey());
		WARNING_TITLE = localeEngine.getStringResource(EKeys.OP_WARNING_MESSAGE.getKey());

		localeEngine.localizeOptionPane();

		localeEngine.localizeFileChooser();
	}

    public void showMessage() {
        Object[] but = {"OK"};
	    int n = JOptionPane.showOptionDialog(mainFrame, NOT_CORRECT_MESSAGE, WARNING_TITLE,
			    JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, but, but[0]);

        if(n == 0) {
            controller.showSettingsMenu();
            controller.checkIsFoldersExist();
            try {
                refreshJTree();
            } catch (BookSystemException e) {
                e.printStackTrace();
            }
        }
    }

	public int showOverwriteDialog (File newFile) {

		String confirmMessage =
				String.format("%s \n %s", OVERWRITE_MESSAGE, newFile);
		return JOptionPane.showConfirmDialog(mainFrame, confirmMessage);
	}

}
