package main.java.com.javarush.listener;

import main.java.com.javarush.controller.MainController;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

public class CategoriesTreeModelListener implements TreeModelListener {
    private MainController controller;

    public CategoriesTreeModelListener(MainController controller) {
        this.controller = controller;
    }

    public void treeNodesChanged(TreeModelEvent tme) {

        TreePath tp = tme.getTreePath();
        Object[] children = tme.getChildren();
        DefaultMutableTreeNode changedNode;
        if (children != null)
            changedNode = (DefaultMutableTreeNode) children[0];
        else
            changedNode = (DefaultMutableTreeNode) tp.getLastPathComponent();

        String strDirectory;
        String nameDirectory = changedNode.getUserObject().toString();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < tp.getPathCount(); i++)
            sb.append(tp.getPathComponent(i)).append("/");

        if (!controller.isDirectoryNameValid(nameDirectory)) {
            return;
        }

        strDirectory = String.valueOf(sb.append(nameDirectory));
        controller.createFileOnFileSystem(strDirectory);
    }

    public void treeNodesInserted(TreeModelEvent tme) {
    }

    public void treeNodesRemoved(TreeModelEvent tme) {
    }

    public void treeStructureChanged(TreeModelEvent tme) {
    }
}
