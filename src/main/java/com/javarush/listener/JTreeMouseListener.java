package main.java.com.javarush.listener;

import main.java.com.javarush.controller.MainController;

import javax.swing.*;
import javax.swing.tree.TreePath;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created with IntelliJ IDEA.
 * User: dj
 * Date: 17.01.14
 * Time: 22:43
 */
public class JTreeMouseListener implements MouseListener {
    private MainController controller;

    public JTreeMouseListener(MainController controller) {
        this.controller = controller;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        JTree tree = (JTree) e.getSource();
        int selRow = tree.getRowForLocation(e.getX(), e.getY());
        TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());

        if (SwingUtilities.isRightMouseButton(e)) {
            if (selRow != -1) {
                tree.setSelectionPath(selPath);
                showPopupMenu(e);
            }
        } else
            if (SwingUtilities.isLeftMouseButton(e)) {
                if (e.getClickCount() == 2) {
                    if (selPath == null) return;
                    if (selPath.getPathCount() <= 2) return;
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < selPath.getPathCount(); i++)
                        sb.append(selPath.getPathComponent(i)).append("/");
                    controller.scanBooks(sb.toString());
                    controller.getView().getTabbedPane().setSelectedIndex(0);
                }
            }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        JTree tree = (JTree) e.getSource();
        int selRow = tree.getRowForLocation(e.getX(), e.getY());
        if (selRow != -1) {
            showPopupMenu(e);
        }
    }

    private void showPopupMenu(MouseEvent e) {
        if (e.isPopupTrigger()) {
            controller.showCategoriesPopupMenu(e.getComponent(), e.getX(), e.getY());
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }
}
