package main.java.com.javarush.listener;

import main.java.com.javarush.controller.MainController;
import main.java.com.javarush.exceptions.BookSystemException;

import javax.swing.*;
import javax.swing.tree.TreePath;
import java.awt.datatransfer.DataFlavor;
import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class DragAndDropHandler extends TransferHandler {

    private static final String OVERWRITE_MESSAGE = "Do you want to overwrite existing file?";
	private static java.util.List<File> writtenFiles;
	private MainController controller;
	private JFrame mainFrame;
    private boolean isFavorite = false;
	
    public DragAndDropHandler(MainController controller, JFrame frame) {
        this.controller = controller;
	    this.mainFrame = frame;
    }

    public boolean canImport(TransferSupport info) {
        JTree.DropLocation jtdl = (JTree.DropLocation) info.getDropLocation();

        try {
            //import denied in root dir
            Object[] ss = jtdl.getPath().getPath();
            if (ss.length <= 2) {
                return false;
            }

            //import denied for non-book files
            java.util.List data = (java.util.List) info.getTransferable()
                    .getTransferData(DataFlavor.javaFileListFlavor);
            for (Object d : data) {
                File file = (File) d;
                if (!isBook(file.getName())) return false;
            }

        } catch (NullPointerException e) {
            return false;
        } catch (Exception e) {
            return false;
        }

        //allowed only for files
        return info.isDataFlavorSupported(DataFlavor.javaFileListFlavor);

    }

    public boolean importData(TransferSupport info) {
        if (!info.isDrop()) {
            return false;
        }

        //letting working DND only with files
        if (!info.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
            return false;
        }

        JTree.DropLocation jtdl = (JTree.DropLocation) info.getDropLocation();
        TreePath p = jtdl.getPath();
        Object[] pathElements;

        try {
            pathElements = p.getPath();

            //if droop being done in root directory it's false
            if (pathElements.length <= 2) {
                return false;
            }
        } catch (NullPointerException e) {
            return false;
        }

	    String newFilePath = getDropPath(p);

        try {
            //getting list of files
            java.util.List data = (java.util.List) info.getTransferable()
                                    .getTransferData(DataFlavor.javaFileListFlavor);
            writtenFiles = new ArrayList<File>();

	        for (Object d : data) {

                File file = (File) d;
                //import denied for non-book files
                if (!isBook(file.getName())) return false;

	            int userChoice;
	            String fileName = file.getName();
	            File newFile = new File(newFilePath, fileName);

		        if (file.getAbsolutePath().equals(newFile.getAbsolutePath())) {continue;}
		        
	            if(newFile.exists()) {

//		            String overwriteMassage = WidgetFactory.getOverwriteMassageLabel().getText();
//		            String confirmMessage =
//				            String.format("%s \n %s", OVERWRITE_MESSAGE, newFile);
		            userChoice = controller.showOverwriteMassage(newFile);

		            if (userChoice == JOptionPane.CANCEL_OPTION) {
			            break;
		            } else if (userChoice == JOptionPane.CLOSED_OPTION) {
			            break;
		            } else if (userChoice == JOptionPane.NO_OPTION) {
			            continue;
		            } else if (userChoice == JOptionPane.YES_OPTION) {
			            boolean deleted = newFile.delete();
		            }
	            }
		        
		        writtenFiles.add(file);

                if (info.getDropAction() == MOVE) {
                    boolean renamed = file.renameTo(newFile);
                    indexBook(file, newFile);
                    continue;
                }

                Files.copy(file.toPath(), newFile.toPath());
		        indexBook(file, newFile);       //index file
                //todo: adding  progress bar when files rewriting
            }

            //if there was a move action then files will be deleted
            if (info.getDropAction() == MOVE) {
                for (File f : writtenFiles) {
                    boolean deleted = f.delete();
                }
            }
            return true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

	private void indexBook(File file, File newFile) throws BookSystemException {
        controller.processIndex(file, newFile);
    }

    //checks if file is book
    private boolean isBook(String name) throws BookSystemException {
        return controller.hasFileSpecifiedBookExtension(name);
    }

	/**
	 * Getting path where we will place files
	 * @param p - Path from JTree node;
	 * @return string of system path to drop a file.
	 */
	private String getDropPath(TreePath p) {

		StringBuilder dropPath = new StringBuilder();


		for (int i = 0; i < p.getPathCount(); i++) {
			String s = p.getPathComponent(i).toString();
			dropPath.append(s);
			if ('\\' != s.charAt(s.length() - 1)) {
				dropPath.append("/");
			}
		}
		return dropPath.toString();
	}

	public static List<File> getWrittenFiles() {
		return writtenFiles;
	}
}
