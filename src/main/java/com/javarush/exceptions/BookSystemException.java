package main.java.com.javarush.exceptions;

public class BookSystemException extends Exception {
    public BookSystemException(String message) {
        super(message);
    }

    public BookSystemException(String message, Throwable cause) {
        super(message, cause);
    }
}
