package main.java.com.javarush.view.component.table;

import main.java.com.javarush.controller.MainController;
import main.java.com.javarush.view.book.BookIcon;

import javax.swing.table.AbstractTableModel;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FavoriteBookTableModel extends AbstractTableModel{
    private List<File> favorite = new ArrayList<File>();
    private MainController controller;

    public FavoriteBookTableModel(MainController controller) {
        this.controller = controller;
    }

    public void refreshData(List<File> books) {
        this.favorite = books;
        fireTableDataChanged();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int c) {
        String fieldName = null;
        if (c == 1)
            fieldName = "name";
        else
        if (c == 0)
            fieldName = "type";
        return fieldName;
    }

    @Override
    public Object getValueAt(int r, int c) {

        String name = favorite.get(r).getName();
        if (c == 1) {
            return name.substring(0, name.lastIndexOf(""));
        } else {
            return BookIcon.getIcon(name);
        }
    }

    public boolean isCellEditable(int row, int col) {
        return col == 1 && controller.getModel().isTableEditable();
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        File file = controller.renameFile(aValue, rowIndex);
        favorite.set(rowIndex, file);
        fireTableCellUpdated(rowIndex, columnIndex);
        controller.getModel().setTableEditable(false);
    }

    @Override
    public int getRowCount() {
        return controller.getRowCountOnOnePage();
    }

    public File getBookByRowIndex(int rowIndex) {
        return favorite.get(rowIndex);
    }
}
