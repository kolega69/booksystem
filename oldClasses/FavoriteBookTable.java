package main.java.com.javarush.view.component.table;


import main.java.com.javarush.controller.MainController;

import java.io.File;
import java.util.List;

public class FavoriteBookTable extends JTable {
    private final FavoriteBookTableModel tableModel;
    private final MainController controller;

    public FavoriteBookTable(final MainController controller) {
        this.controller = controller;
        this.tableModel = new FavoriteBookTableModel(controller);
        this.setModel(tableModel);
    }

    public void init() {
        addMouseListener(new MouseCommands(controller));

        setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        int width = 40;
        getColumnModel().getColumn(0).setMinWidth(width);
        getColumnModel().getColumn(0).setMaxWidth(width);
        getColumnModel().getColumn(0).setPreferredWidth(width);
        setRowHeight(width);
        getColumnModel().getColumn(0).setCellRenderer(new JTableIconRenderer());
        getColumnModel().getColumn(1).setMinWidth(638);
        getColumnModel().getColumn(1).setMinWidth(638);
    }

    public void refreshData(List<File> files) {
        tableModel.refreshData(files);
    }

    public void renameSelectedFile() {
        controller.getModel().setTableEditable(true);

        String s = (String)getValueAt(getSelectedRow(), getSelectedColumn());

        JTextField tf = new JTextField(s);
        tf.setEditable(true);
        tf.setSelectedTextColor(Color.BLUE);

        getColumnModel().getColumn(getSelectedColumn()).setCellEditor(new DefaultCellEditor(tf));

        editCellAt(getSelectedRow(), getSelectedColumn());

        tf.requestFocus();
        tf.setSelectionStart(0);
        tf.setSelectionEnd(s.length());
    }
}
